# Faraday

#### ICS Project in .NET _(Summer 2019)_

## Setup

- Download [SQLlocalDB.msi](https://www.microsoft.com/en-us/download/details.aspx?id=42299) file and install
  - To check and manage DB download [SSMS](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017)
  - Connect to the local DB **Server name:** ```(LocalDB)\MSSQLLocalDB```
  - _Application DB is not present yet_
- Setup you startup project to **Faraday.DAL.Tests**
- Open ```View > Other Windows > Package Manager Console```
  - In ```Package Manager Console``` set default project to **Faraday.DAL**
  - Use command ```update-databse```
- Inside SSMS refresh Database and **Faraday** DB should be present
- Now it is ready to rock on the local machine

- If application is not working and file ```appSettings.json``` is not present inside application assambly:
  - Copy ```appSettings.json``` from **Faraday.DAL.Tests** to the application assembly
  - Set file property ```Copy to Output Directory | Copy always```
  - Now should be application ready