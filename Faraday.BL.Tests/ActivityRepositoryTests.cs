﻿using System.Collections.Generic;
using System.Linq;
using Faraday.BL.Model;
using Faraday.DAL.Entities;
using FluentAssertions;
using Xunit;

namespace Faraday.BL.Tests
{
    public class ActivityRepositoryTests : RepositoryTests
    {
        public static IEnumerable<object[]> GetUsers()
        {
            yield return new object[]
            {
                new UserModel
                {
                    Email = "mymail@mail.com",
                    Login = "Bob",
                    Name = "Bobek",
                    Password = "bestpaswever"
                }
            };
        }

        public static IEnumerable<object[]> GetUsersAndTeams()
        {
            yield return new object[]
            {
                new UserModel
                {
                    Email = "yourmail@mail.com",
                    Login = "Dave",
                    Name = "Bobek",
                    Password = "bestpaswever"
                },
                new TeamModel
                {
                    Name = "Team Bobek",
                    Description = "The cutest team ever!"
                }
            };
        }

        //[Theory]
        //[MemberData(nameof(GetUsers))]
        public void CreatedByActivityTest(UserModel model)
        {
            var insertedModel = Sut.User.Insert(model);

            var activity = Sut.Activity.GetLastByUser(insertedModel);

            Assert.Equal(insertedModel.Id, activity?.CreatedById);

            Sut.User.Remove(insertedModel);
        }

        //[Theory]
        //[MemberData(nameof(GetUsers))]
        public void CreatedInsertActivityTest(UserModel model)
        {
            var insertedModel = Sut.User.Insert(model);

            var activity = Sut.Activity.GetLastByUser(insertedModel);

            Assert.Equal(insertedModel.Id, activity?.UserId);
            Assert.Equal(Activity.Type.Insert, activity?.Type);

            Sut.User.Remove(insertedModel);
        }

        //[Theory]
        //[MemberData(nameof(GetUsers))]
        public void CreatedUpdateActivityTest(UserModel model)
        {
            var modifiedName = model.Name + " suffix";
            var insertedUser = Sut.User.Insert(model);

            insertedUser.Name = modifiedName;
            var updatedUser = Sut.User.Update(insertedUser);

            var activity = Sut.Activity.GetLastByUser(updatedUser);

            Assert.Equal(updatedUser.Id, activity?.UserId);
            Assert.Equal(Activity.Type.Update, activity?.Type);

            Sut.User.Remove(updatedUser);
        }

        //[Theory]
        //[MemberData(nameof(GetUsersAndTeams))]
        public void CreatedDeleteActivityTest(UserModel user, TeamModel team)
        {
            var insertedUser = Sut.User.Insert(user);
            team.CreatedBy = insertedUser;
            var insertedTeam = Sut.Team.Insert(team);

            var removedModel = Sut.Team.Remove(insertedTeam);
            var activity = Sut.Activity.GetLastByUser(insertedUser);

            Assert.Equal(removedModel.Id, activity?.TeamId);
            Assert.Equal(Activity.Type.Delete, activity?.Type);
        }

        //[Theory]
        //[MemberData(nameof(GetUsersAndTeams))]
        public void GetLastActivitiesTest(UserModel user, TeamModel team)
        {
            var insertedUser = Sut.User.Insert(user);
            team.CreatedBy = insertedUser;
            var insertedTeam = Sut.Team.Insert(team);
            var removedTeam = Sut.Team.Remove(insertedTeam);

            var activities = Sut.Activity.GetLastByUser(insertedUser, 5).ToList();
            var recentActivity = activities.First();

            Assert.Equal(3, activities.Count);
            Assert.Equal(removedTeam.Id, recentActivity?.TeamId);
            Assert.Equal(Activity.Type.Delete, recentActivity?.Type);
        }

        //[Theory]
        //[MemberData(nameof(GetUsersAndTeams))]
        public void GetAuthorTest(UserModel user, TeamModel team)
        {
            var insertedUser = Sut.User.Insert(user);
            team.CreatedBy = insertedUser;
            Sut.Team.Insert(team);

            var activity = Sut.Activity.GetLastByUser(insertedUser);
            Sut.Activity.LoadAuthor(activity);

            activity.CreatedBy.Should().BeEquivalentTo(insertedUser);
        }
    }
}