﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Faraday.BL.Model;
using FluentAssertions;
using Xunit;

namespace Faraday.BL.Tests
{
    public class TeamRepositoryTests : RepositoryTests
    {
        public static IEnumerable<object[]> GetTeams()
        {
            yield return new object[]
            {
                new TeamModel
                {
                    Name = "First tested cool team",
                    Description = "just a team for testing"
                }
            };
            yield return new object[]
            {
                new TeamModel()
            };
        }

        [Theory]
        [MemberData(nameof(GetTeams))]
        public void GetAllTest(TeamModel model)
        {
            Sut.Team.Insert(model);
            Sut.Team.Insert(model); // InMemory DB ignores unique constraint

            var models = Sut.Team.GetAll();

            models.Should().HaveCountGreaterOrEqualTo(2);
        }

        [Theory]
        [MemberData(nameof(GetTeams))]
        public void GetByIdTest(TeamModel model)
        {
            var createdTeam = Sut.Team.Insert(model);

            var returnedModel = Sut.Team.GetById(createdTeam.Id);
            Assert.NotNull(returnedModel);

            Sut.Team.Remove(createdTeam.Id);
        }

        [Theory]
        [MemberData(nameof(GetTeams))]
        public void RemoveTest(TeamModel model)
        {
            var createdTeam = Sut.Team.Insert(model);

            Sut.Team.Remove(createdTeam.Id);

            Assert.Null(Sut.Team.GetById(createdTeam.Id));
        }

        [Theory]
        [MemberData(nameof(GetTeams))]
        public void UpdateTest(TeamModel model)
        {
            var updatedName = model.Name + " suffix";
            var createdTeam = Sut.Team.Insert(model);

            createdTeam.Name = updatedName;
            Sut.Team.Update(createdTeam);

            var updatedTeam = Sut.Team.GetById(createdTeam.Id);
            Assert.True(updatedTeam.Name == updatedName);

            Sut.Team.Remove(createdTeam.Id);
        }

        [Theory]
        [MemberData(nameof(GetTeams))]
        public void LoadTeamMembersTest(TeamModel model)
        {
            var iUser1 = Sut.User.Insert(new UserModel());
            var iUser2 = Sut.User.Insert(new UserModel());
            model.CreatedBy = iUser1;
            var iTeam = Sut.Team.Insert(model);

            var iTeamMember1 = Sut.TeamMember.Insert(new TeamMemberModel
            {
                CreatedBy = iUser1,
                Member = iUser1,
                Team = iTeam
            });
            var iTeamMember2 = Sut.TeamMember.Insert(new TeamMemberModel
            {
                CreatedBy = iUser1,
                Member = iUser2,
                Team = iTeam
            });
            var insertedTeamMembers = new Collection<TeamMemberModel>
            {
                iTeamMember1,
                iTeamMember2
            };

            Assert.Null(iTeam.Members);
            Sut.Team.LoadTeamMembers(iTeam);
            iTeam.Members.Should().HaveCount(2).And.BeEquivalentTo(insertedTeamMembers);
        }

        [Theory]
        [MemberData(nameof(GetTeams))]
        public void LoadPostsTest(TeamModel model)
        {
            var iUser = Sut.User.Insert(new UserModel());
            model.CreatedBy = iUser;
            var iTeam = Sut.Team.Insert(model);

            var postModel = new PostModel
            {
                Title = "",
                Body = "",
                CreatedBy = iUser,
                Team = iTeam
            };
            var iPost1 = Sut.Post.Insert(postModel);
            var iPost2 = Sut.Post.Insert(postModel);
            var iPosts = new Collection<PostModel>
            {
                iPost1,
                iPost2
            };

            Assert.Null(iTeam.Posts);
            Sut.Team.LoadPosts(iTeam);
            iTeam.Posts.Should().HaveCount(2).And.BeEquivalentTo(iPosts);
        }
    }
}