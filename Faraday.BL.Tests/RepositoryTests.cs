﻿using Faraday.BL.Repository;

namespace Faraday.BL.Tests
{
    public abstract class RepositoryTests
    {
        public Repositories Sut { get; }

        protected RepositoryTests()
        {
            var inMemoryDb = new InMemoryDbContextFactory();
            Sut = new Repositories
            {
                User = new UserRepository(inMemoryDb),
                Team = new TeamRepository(inMemoryDb),
                TeamMember = new TeamMemberRepository(inMemoryDb),
                Post = new PostRepository(inMemoryDb),
                Comment = new CommentRepository(inMemoryDb),
                Avatar = new AvatarRepository(inMemoryDb),
                Activity = new ActivityRepository(inMemoryDb)
            };
        }
    }

    public class Repositories
    {
        public UserRepository User { get; set; }
        public TeamRepository Team { get; set; }
        public TeamMemberRepository TeamMember { get; set; }
        public PostRepository Post { get; set; }
        public CommentRepository Comment { get; set; }
        public AvatarRepository Avatar { get; set; }
        public ActivityRepository Activity { get; set; }
    }
}