using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Faraday.BL.Model;
using FluentAssertions;
using Xunit;

namespace Faraday.BL.Tests
{
    public class UserRepositoryTests : RepositoryTests
    {
        public static IEnumerable<object[]> GetUsers()
        {
            yield return new object[]
            {
                new UserModel
                {
                    Email = "mymail@mail.com",
                    Login = "Bob",
                    Name = "Bobek",
                    Password = "bestpaswever"
                }
            };
            yield return new object[]
            {
                new UserModel
                {
                    Email = "myMail2@mail.com",
                    Login = "Bob2",
                    Name = "Bobek",
                    Password = "bestpaswever"
                }
            };
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void GetAllTest(UserModel model)
        {
            Sut.User.Insert(model);
            Sut.User.Insert(model); // InMemory DB ignores unique constraint

            var models = Sut.User.GetAll();

            models.Should().HaveCountGreaterOrEqualTo(2);
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void GetByIdTest(UserModel model)
        {
            var insertedModel = Sut.User.Insert(model);

            var foundModel = Sut.User.GetById(insertedModel.Id);

            Assert.NotNull(foundModel);

            Sut.User.Remove(insertedModel.Id);
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void GetByIdLogin(UserModel model)
        {
            var insertedModel = Sut.User.Insert(model);

            var foundModel = Sut.User.GetByLogin(insertedModel.Login);

            Assert.NotNull(foundModel);

            Sut.User.Remove(insertedModel.Id);
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void UpdateTest(UserModel model)
        {
            var modifiedName = model.Name + " suffix";
            var insertedUser = Sut.User.Insert(model);

            insertedUser.Name = modifiedName;
            Sut.User.Update(insertedUser);

            var updatedUser = Sut.User.GetById(insertedUser.Id);
            Assert.True(updatedUser.Name == modifiedName);

            Sut.User.Remove(insertedUser.Id);
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void RemoveTest(UserModel model)
        {
            var insertedModel = Sut.User.Insert(model);

            Sut.User.Remove(insertedModel);

            Assert.Null(Sut.User.GetById(insertedModel.Id));
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void RemoveByIdTest(UserModel model)
        {
            var insertedModel = Sut.User.Insert(model);

            Sut.User.Remove(insertedModel.Id);

            Assert.Null(Sut.User.GetById(insertedModel.Id));
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void LoadTeamsTest(UserModel model)
        {
            var iUser = Sut.User.Insert(model);
            var iTeam1 = Sut.Team.Insert(new TeamModel {CreatedBy = iUser});
            var iTeam2 = Sut.Team.Insert(new TeamModel {CreatedBy = iUser});
            var iTeamMember1 = Sut.TeamMember.Insert(new TeamMemberModel
            {
                CreatedBy = iUser,
                Member = iUser,
                Team = iTeam1
            });
            var iTeamMember2 = Sut.TeamMember.Insert(new TeamMemberModel
            {
                CreatedBy = iUser,
                Member = iUser,
                Team = iTeam2
            });
            var iTeamMembers = new Collection<TeamMemberModel>
            {
                iTeamMember1,
                iTeamMember2
            };

            Assert.Null(iUser.MemberOf);
            Sut.User.LoadTeams(iUser);
            iUser.MemberOf.Should().HaveCount(2).And.BeEquivalentTo(iTeamMembers);
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void LoadNoTeamsTest(UserModel model)
        {
            var iUser = Sut.User.Insert(model);

            Assert.Null(iUser.MemberOf);
            Sut.User.LoadTeams(iUser);

            iUser.MemberOf.Should().HaveCount(0);
        }

        //[Theory]
        //[MemberData(nameof(GetUsers))]
        public void LoadActivitiesTest(UserModel model)
        {
            var iUser = Sut.User.Insert(model);
            var iTeam1 = Sut.Team.Insert(new TeamModel {CreatedBy = iUser});
            var iTeam2 = Sut.Team.Insert(new TeamModel {CreatedBy = iUser});
            Sut.TeamMember.Insert(new TeamMemberModel
            {
                CreatedBy = iUser,
                Member = iUser,
                Team = iTeam1
            });
            Sut.TeamMember.Insert(new TeamMemberModel
            {
                CreatedBy = iUser,
                Member = iUser,
                Team = iTeam2
            });

            Assert.Null(iUser.Activities);
            Sut.User.LoadActivities(iUser);
            var activities = Sut.Activity.GetLastByUser(iUser, 5);
            iUser.Activities.Should().HaveCount(5).And.BeEquivalentTo(activities);
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void LoadAvatarTest(UserModel model)
        {
            model.Picture = new AvatarModel
            {
                ProfilePicture = new byte[] {3}
            };
            var iUser = Sut.User.Insert(model);
            iUser.Picture.CreatedById = iUser.Id;
            var picture = Sut.Avatar.Update(iUser.Picture);

            iUser.Picture = null;
            iUser.PictureId = null;
            Sut.User.LoadAvatar(iUser);

            Assert.NotNull(iUser.Picture);
            iUser.Picture.Should().BeEquivalentTo(picture, options => options.Excluding(a => a.CreatedBy));
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void EmailExistsTest(UserModel model)
        {
            var iUser = Sut.User.Insert(model);

            var exists = Sut.User.EmailExists(iUser.Email);

            Assert.True(exists);
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void LoginExistsTest(UserModel model)
        {
            var iUser = Sut.User.Insert(model);

            var exists = Sut.User.LoginExists(iUser.Login);

            Assert.True(exists);
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void EmailNotExistsTest(UserModel model)
        {
            model.Email = "prefix." + model.Email;
            var user1 = new UserModel
            {
                Email = "x1." + model.Email
            };
            var user2 = new UserModel
            {
                Email = "x2." + model.Email
            };

            Sut.User.Insert(user1);
            Sut.User.Insert(user2);

            var exists = Sut.User.EmailExists(model.Email);

            Assert.False(exists);
        }

        [Theory]
        [MemberData(nameof(GetUsers))]
        public void LoginNotExistsTest(UserModel model)
        {
            model.Login += "_suffix";
            var user1 = new UserModel
            {
                Login = model.Login + "_1"
            };
            var user2 = new UserModel
            {
                Login = model.Login + "_2"
            };

            Sut.User.Insert(user1);
            Sut.User.Insert(user2);

            var exists = Sut.User.LoginExists(model.Login);

            Assert.False(exists);
        }
    }
}