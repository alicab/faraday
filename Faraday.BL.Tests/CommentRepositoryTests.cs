﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Faraday.BL.Model;
using FluentAssertions;
using Xunit;

namespace Faraday.BL.Tests
{
    public class CommentRepositoryTests : RepositoryTests
    {
        public static IEnumerable<object[]> GetComments()
        {
            yield return new object[]
            {
                new CommentModel
                {
                    Body = ""
                }
            };
        }

        public static IEnumerable<object[]> GetCommentsAndSearchTexts()
        {
            yield return new object[]
            {
                new CommentModel
                {
                    Body = "Random text with few words"
                },
                "with few"
            };
        }

        [Theory]
        [MemberData(nameof(GetComments))]
        public void GetAllTest(CommentModel model)
        {
            Sut.Comment.Insert(model);
            Sut.Comment.Insert(model); // InMemory DB ignores unique constraint

            var models = Sut.Comment.GetAll();

            models.Should().HaveCountGreaterOrEqualTo(2);
        }

        [Theory]
        [MemberData(nameof(GetComments))]
        public void GetAllByParentTest(CommentModel model)
        {
            var iParent = Sut.Post.Insert(new PostModel
            {
                Title = "How are you",
                Body = "strasna nuda"
            });
            model.Parent = iParent;
            model.CreatedBy = Sut.User.Insert(new UserModel());

            var iComment1 = Sut.Comment.Insert(model);
            var iComment2 = Sut.Comment.Insert(model);
            var iComments = new Collection<CommentModel>
            {
                iComment1,
                iComment2
            };

            var models = Sut.Comment.GetAllByParent(iParent.Id);
            models.Should().HaveCount(2).And.BeEquivalentTo(iComments);
        }

        [Theory]
        [MemberData(nameof(GetComments))]
        public void GetByIdTest(CommentModel model)
        {
            var createdModel = Sut.Comment.Insert(model);

            var foundModel = Sut.Comment.GetById(createdModel.Id);
            Assert.NotNull(foundModel);

            Sut.Comment.Remove(createdModel.Id);
        }

        [Theory]
        [MemberData(nameof(GetComments))]
        public void RemoveTest(CommentModel model)
        {
            var createdModel = Sut.Comment.Insert(model);

            Sut.Comment.Remove(createdModel.Id);

            Assert.Null(Sut.Comment.GetById(createdModel.Id));
        }

        [Theory]
        [MemberData(nameof(GetComments))]
        public void UpdateTest(CommentModel model)
        {
            var updatedBody = model.Body + " suffix";
            var createdComment = Sut.Comment.Insert(model);

            createdComment.Body = updatedBody;
            Sut.Comment.Update(createdComment);

            var updatedComment = Sut.Comment.GetById(createdComment.Id);
            Assert.True(updatedComment.Body == updatedBody);
            Assert.True(updatedComment.IsModified);

            Sut.Comment.Remove(createdComment.Id);
        }

        [Theory]
        [MemberData(nameof(GetCommentsAndSearchTexts))]
        public void SearchTest(CommentModel model, string text)
        {
            model.CreatedBy = Sut.User.Insert(new UserModel());
            var iComment = Sut.Comment.Insert(model);

            var foundComment = Sut.Comment.Search(text);

            var result = iComment.Body == foundComment.Body;
            Assert.True(result);

            Sut.Comment.Remove(iComment);
        }

        [Theory]
        [MemberData(nameof(GetComments))]
        public void GetAllByAuthorTest(CommentModel model)
        {
            var userOwner = new UserModel
            {
                Email = "mymail@mail.com",
                Login = "Bob",
                Name = "Bobek",
                Password = "bestpaswever"
            };
            var iUser = Sut.User.Insert(userOwner);
            model.CreatedBy = iUser;

            var iComment1 = Sut.Comment.Insert(model);
            var iComment2 = Sut.Comment.Insert(model);
            var iComments = new Collection<CommentModel>
            {
                iComment1,
                iComment2
            };

            var models = Sut.Comment.GetAllByAuthor(iUser);
            models.Should().HaveCount(2).And.BeEquivalentTo(iComments);
        }
    }
}