﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Faraday.BL.Model;
using FluentAssertions;
using Xunit;

namespace Faraday.BL.Tests
{
    public class PostRepositoryTests : RepositoryTests
    {
        public static IEnumerable<object[]> GetPosts()
        {
            yield return new object[]
            {
                new PostModel()
                {
                    Title = "How are you",
                    Body = "strasna nuda"
                }
            };
        }

        public static IEnumerable<object[]> GetPostsAndBodySearchTexts()
        {
            yield return new object[]
            {
                new PostModel()
                {
                    Title = "How are you",
                    Body = "strasna nuda"
                },
                "nuda"
            };
        }

        public static IEnumerable<object[]> GetPostsAndTitleSearchTexts()
        {
            yield return new object[]
            {
                new PostModel()
                {
                    Title = "How are you",
                    Body = "strasna nuda"
                },
                "are"
            };
        }

        public static IEnumerable<object[]> GetPostsAndSearchTexts()
        {
            yield return new object[]
            {
                new PostModel()
                {
                    Title = "How are you",
                    Body = "strasna nuda"
                },
                "nuda"
            };
            yield return new object[]
            {
                new PostModel()
                {
                    Title = "How are you",
                    Body = "strasna nuda"
                },
                "are"
            };
        }

        [Theory]
        [MemberData(nameof(GetPosts))]
        public void GetAllTest(PostModel model)
        {
            Sut.Post.Insert(model);
            Sut.Post.Insert(model); // InMemory DB ignores unique constraint

            var models = Sut.Post.GetAll();

            models.Should().HaveCountGreaterOrEqualTo(2);
        }

        [Theory]
        [MemberData(nameof(GetPosts))]
        public void GetByIdTest(PostModel model)
        {
            var createdModel = Sut.Post.Insert(model);

            var foundModel = Sut.Post.GetById(createdModel.Id);
            Assert.NotNull(foundModel);

            Sut.Post.Remove(createdModel.Id);
        }

        [Theory]
        [MemberData(nameof(GetPosts))]
        public void UpdateTest(PostModel model)
        {
            var updateTitle = model.Title + " suffix";
            var createdPost = Sut.Post.Insert(model);

            createdPost.Title = updateTitle;
            Sut.Post.Update(createdPost);

            var updatedPost = Sut.Post.GetById(createdPost.Id);
            Assert.True(updatedPost.Title == updateTitle);
            Assert.True(updatedPost.IsModified);

            Sut.Post.Remove(createdPost.Id);
        }

        [Theory]
        [MemberData(nameof(GetPosts))]
        public void GetAllByTeamTest(PostModel model)
        {
            var iTeam = Sut.Team.Insert(new TeamModel());
            model.Team = iTeam;
            model.CreatedBy = Sut.User.Insert(new UserModel());

            var iPost1 = Sut.Post.Insert(model);
            var iPost2 = Sut.Post.Insert(model);
            var iPosts = new Collection<PostModel>
            {
                iPost1,
                iPost2
            };

            var models = Sut.Post.GetAllByTeam(iTeam.Id);
            models.Should().HaveCount(2).And.BeEquivalentTo(iPosts);
        }

        [Theory]
        [MemberData(nameof(GetPosts))]
        public void GetAllByAuthorTest(PostModel model)
        {
            var iUser = Sut.User.Insert(new UserModel());
            model.CreatedBy = iUser;

            var iPost1 = Sut.Post.Insert(model);
            var iPost2 = Sut.Post.Insert(model);
            var iPosts = new Collection<PostModel>
            {
                iPost1,
                iPost2
            };

            var models = Sut.Post.GetAllByAuthor(iUser);
            models.Should().HaveCount(2).And.BeEquivalentTo(iPosts);
        }

        [Theory]
        [MemberData(nameof(GetPosts))]
        public void RemoveTest(PostModel model)
        {
            var createdModel = Sut.Post.Insert(model);

            Sut.Post.Remove(createdModel.Id);

            Assert.Null(Sut.Post.GetById(createdModel.Id));
        }

        [Theory]
        [MemberData(nameof(GetPostsAndTitleSearchTexts))]
        public void SearchInTitleTest(PostModel model, string text)
        {
            model.CreatedBy = Sut.User.Insert(new UserModel());
            var iPost = Sut.Post.Insert(model);

            var foundPost = Sut.Post.SearchInTitle(text);

            Assert.Equal(iPost.Title, foundPost.Title);

            Sut.Post.Remove(iPost);
        }

        [Theory]
        [MemberData(nameof(GetPostsAndTitleSearchTexts))]
        public void SearchInTitleTest_Negative(PostModel model, string text)
        {
            var iPost = Sut.Post.Insert(model);

            var foundPost = Sut.Post.SearchInBody(text);

            Assert.Null(foundPost);

            Sut.Post.Remove(iPost);
        }

        [Theory]
        [MemberData(nameof(GetPostsAndBodySearchTexts))]
        public void SearchInBodyTest(PostModel model, string text)
        {
            model.CreatedBy = Sut.User.Insert(new UserModel());
            var iPost = Sut.Post.Insert(model);

            var foundPost = Sut.Post.SearchInBody(text);

            Assert.Equal(iPost.Body, foundPost.Body);

            Sut.Post.Remove(iPost);
        }

        [Theory]
        [MemberData(nameof(GetPostsAndBodySearchTexts))]
        public void SearchInBodyTest_Negative(PostModel model, string text)
        {
            var iPost = Sut.Post.Insert(model);

            var foundPost = Sut.Post.SearchInTitle(text);

            Assert.Null(foundPost);

            Sut.Post.Remove(iPost);
        }

        [Theory]
        [MemberData(nameof(GetPostsAndSearchTexts))]
        public void SearchTest(PostModel model, string text)
        {
            model.CreatedBy = Sut.User.Insert(new UserModel());
            var iPost = Sut.Post.Insert(model);

            var foundPost = Sut.Post.Search(text);

            var result = iPost.Title == foundPost.Title || iPost.Body == foundPost.Body;
            Assert.True(result);

            Sut.Post.Remove(iPost);
        }

        [Theory]
        [MemberData(nameof(GetPosts))]
        public void LoadChildrenWithAuthorTest(PostModel model)
        {
            var iPost = Sut.Post.Insert(model);
            var iUser = Sut.User.Insert(new UserModel());
            var comment = new CommentModel
            {
                Body = "Random text",
                Parent = iPost,
                CreatedBy = iUser
            };

            var iComment1 = Sut.Comment.Insert(comment);
            var iComment2 = Sut.Comment.Insert(comment);
            var iComments = new Collection<CommentModel>
            {
                iComment1,
                iComment2
            };

            Assert.Null(iPost.Children);
            Sut.Post.LoadChildrenWithAuthor(iPost);

            iPost.Children.Should().HaveCount(2).And.BeEquivalentTo(iComments);
        }
    }
}