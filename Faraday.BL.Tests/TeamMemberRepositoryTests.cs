﻿using Faraday.BL.Model;
using Faraday.BL.Repository;
using Microsoft.EntityFrameworkCore.Internal;
using Xunit;

namespace Faraday.BL.Tests
{
    public class TeamMemberRepositoryTests
    {
        TeamMemberRepository CreateSUT()
        {
            return new TeamMemberRepository(new InMemoryDbContextFactory());
        }

        [Fact]
        public void InsertTest()
        {
            var sut = CreateSUT();

            var userModel = new UserModel()
            {
                Id = 1,
                Email = "mymail@mail.com",
                Login = "Bob",
                Name = "Bobek",
                Password = "bestpaswever"
            };

            var teamModel = new TeamModel()
            {
                Id = 1,
                Name = "bumbrlici",
                Description = "GOTO je nejlepsi"
            };

            var teamMemberModel = new TeamMemberModel()
            {
                MemberId = userModel.Id,
                TeamId = teamModel.Id
            };

            TeamMemberModel createdPost = null;

            try
            {
                createdPost = sut.Insert(teamMemberModel);

                Assert.NotNull(createdPost);
            }
            finally
            {
                if (createdPost != null)
                {
                    sut.Remove(createdPost);
                }
            }
        }

        [Fact]
        public void GetAllByUserIdTest()
        {
            var sut = CreateSUT();

            var userModel = new UserModel()
            {
                Id = 1,
                Email = "mymail@mail.com",
                Login = "Bob",
                Name = "Bobek",
                Password = "bestpaswever"
            };

            var teamModel = new TeamModel()
            {
                Id = 1,
                Name = "bumbrlici",
                Description = "GOTO je nejlepsi"
            };

            var teamMemberModel = new TeamMemberModel()
            {
                MemberId = userModel.Id,
                TeamId = teamModel.Id
            };


            TeamMemberModel createdTeamMember = sut.Insert(teamMemberModel);

            try
            {
                var allUserRelations = sut.GetAllByUserId(userModel.Id);
                Assert.True(allUserRelations.Any());
            }
            finally
            {
                if (createdTeamMember != null)
                {
                    sut.Remove(createdTeamMember);
                }
            }
        }

        [Fact]
        public void GetAllByTeamIdTest()
        {
            var sut = CreateSUT();

            var userModel = new UserModel()
            {
                Id = 1,
                Email = "mymail@mail.com",
                Login = "Bob",
                Name = "Bobek",
                Password = "bestpaswever"
            };

            var teamModel = new TeamModel()
            {
                Id = 1,
                Name = "bumbrlici",
                Description = "GOTO je nejlepsi"
            };

            var teamMemberModel = new TeamMemberModel()
            {
                MemberId = userModel.Id,
                TeamId = teamModel.Id
            };


            TeamMemberModel createdTeamMember = sut.Insert(teamMemberModel);

            try
            {
                var allTeamRelations = sut.GetAllByTeamId(userModel.Id);
                Assert.True(allTeamRelations.Any());
            }
            finally
            {
                if (createdTeamMember != null)
                {
                    sut.Remove(createdTeamMember);
                }
            }
        }
    }
}