﻿using System;
using Microsoft.EntityFrameworkCore;
using Faraday.DAL;

namespace Faraday.BL.Tests
{
    public class InMemoryDbContextFactory : IDbContextFactory
    {
        public AppDbContext CreateDbContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseInMemoryDatabase("FaradayDBTest");
            return new AppDbContext(optionsBuilder.Options);
        }
    }
}