﻿namespace Faraday.BL.Tests
{
    public interface IRepositoryTest
    {
        void GetAllTest();
        void InsertTest();
        void GetByIdTest();
        void UpdateTest();
        void RemoveTest();
    }
}