﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using Faraday.BL.Model;
using Faraday.BL.Repository;
using Xunit;

namespace Faraday.BL.Tests
{
    public class AvatarRepositoryTests : IRepositoryTest
    {
        private IRepository<AvatarModel> CreateSUT()
        {
            return new AvatarRepository(new InMemoryDbContextFactory());
        }

        [Fact]
        public void GetAllTest()
        {

        }

        [Fact]
        public void InsertTest()
        {
            var sut = CreateSUT();

            var model = new AvatarModel()
            {
                ProfilePicture = new byte[] {1, 2, 3, 4, 5, 6}
            };

            AvatarModel createdAvatar = null;

            try
            {
                createdAvatar = sut.Insert(model);

                Assert.NotNull(createdAvatar);
            }
            finally
            {
                if (createdAvatar != null)
                {
                    sut.Remove(createdAvatar.Id);
                }
            }
        }

        [Fact]
        public void GetByIdTest()
        {
            var sut = CreateSUT();

            var model = new AvatarModel()
            {
                ProfilePicture = new byte[] {1, 2, 3, 4, 5, 6}
            };

            var createdModel = sut.Insert(model);

            try
            {
                var foundModel = sut.GetById(createdModel.Id);
                Assert.NotNull(foundModel);
            }
            finally
            {
                sut.Remove(createdModel.Id);
            }
        }

        [Fact]
        public void UpdateTest()
        {
            var sut = CreateSUT();

            var model = new AvatarModel()
            {
                ProfilePicture = new byte[] {1, 2, 3, 4, 5, 6}
            };

            var createdAvatar = sut.Insert(model);

            for (int i = 0; i < createdAvatar.ProfilePicture.Length; i++)
            {
                createdAvatar.ProfilePicture[i] = (byte) (6 - i);
            }

            try
            {
                sut.Update(createdAvatar);
                var updatedUser = sut.GetById(createdAvatar.Id);
                Assert.True(createdAvatar.ProfilePicture[0] == 6);
            }
            finally
            {
                sut.Remove(createdAvatar.Id);
            }
        }

        [Fact]
        public void RemoveTest()
        {
            var sut = CreateSUT();

            var model = new AvatarModel()
            {
                ProfilePicture = new byte[] {1, 2, 3, 4, 5, 6}
            };

            var createdModel = sut.Insert(model);

            sut.Remove(createdModel.Id);

            Assert.Null(sut.GetById(createdModel.Id));
        }
    }
}