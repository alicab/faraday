﻿using Microsoft.EntityFrameworkCore;

namespace Faraday.DAL.Tests
{
    public class InMemoryDbContextFactory : IDbContextFactory
    {
        public AppDbContext CreateDbContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseInMemoryDatabase("FaradayDBTest");
            return new AppDbContext(optionsBuilder.Options);
        }
    }
}