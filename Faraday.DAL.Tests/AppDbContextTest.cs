﻿using Faraday.DAL.Entities;
using System.Linq;
using Xunit;

namespace Faraday.DAL.Tests
{
    public class AppDbContextTest
    {
        private readonly IDbContextFactory dbContextFactory;

        public AppDbContextTest()
        {
            dbContextFactory = new InMemoryDbContextFactory();
        }

        [Fact]
        public void CreateUserTest()
        {
            var user = new User
            {
                Name = "Testovaci Panak",
                Login = "TP",
                Email = "mymail@mail.com"
            };

            using (var dbContext = dbContextFactory.CreateDbContext())
            {
                dbContext.Users.Add(user);
                dbContext.SaveChanges();

                var retrievedUser = dbContext.Users.FirstOrDefault(t => t.Id == user.Id);
                Assert.NotNull(retrievedUser);

                dbContext.Remove(retrievedUser);
            }
        }

        [Fact]
        public void CreateTeamTest()
        {
            var user = new User
            {
                Name = "Testovaci Panak",
                Login = "TP",
                Email = "mymail@mail.com"
            };
            var team = new Team
            {
                Name = "BestTeamEver",
                CreatedBy = user
            };

            using (var dbContext = dbContextFactory.CreateDbContext())
            {
                dbContext.Teams.Add(team);
                dbContext.SaveChanges();

                var retrievedTeam = dbContext.Teams.FirstOrDefault(t => t.Id == team.Id);
                Assert.NotNull(retrievedTeam);
                Assert.Equal(user.Id, retrievedTeam.CreatedById);

                dbContext.Remove(retrievedTeam);
            }
        }

        [Fact]
        public void CreateAvatarTest()
        {
            var avatar = new Avatar();

            using (var dbContext = dbContextFactory.CreateDbContext())
            {
                dbContext.Avatars.Add(avatar);
                dbContext.SaveChanges();

                var retrievedAvatar = dbContext.Avatars.FirstOrDefault(t => t.Id == avatar.Id);
                Assert.NotNull(retrievedAvatar);

                dbContext.Remove(retrievedAvatar);
            }
        }
    }
}