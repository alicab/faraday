using Faraday.APP.Services;
using Faraday.APP.ViewModels;
using Faraday.APP.ViewModels.Comment;
using Faraday.APP.ViewModels.Pages;
using Faraday.APP.ViewModels.Post;
using Faraday.APP.ViewModels.Team;
using Faraday.APP.ViewModels.User;
using Faraday.BL.Messenger;
using Faraday.BL.Repository;
using Faraday.DAL;

namespace Faraday.APP
{
    public class ViewModelLocator
    {
        private static readonly Messenger _messenger = Messenger.Instance;
        private static readonly IDbContextFactory AppDbContextFactory = new AppDbContextFactory();

        #region Repositories

        private readonly AvatarRepository _avatarRepository
            = new AvatarRepository(AppDbContextFactory);

        private readonly CommentRepository _commentRepository
            = new CommentRepository(AppDbContextFactory);

        private readonly PostRepository _postRepository
            = new PostRepository(AppDbContextFactory);

        private readonly TeamRepository _teamRepository
            = new TeamRepository(AppDbContextFactory);

        private readonly UserRepository _userRepository
            = new UserRepository(AppDbContextFactory);

        private readonly TeamMemberRepository _teamMemberRepository
            = new TeamMemberRepository(AppDbContextFactory);

        #endregion

        #region MainViewModel

        private MainViewModel _mainViewModel;
        public MainViewModel MainViewModel => _mainViewModel ?? (_mainViewModel = CreateMainViewModel());

        private static MainViewModel CreateMainViewModel()
        {
            return new MainViewModel(_messenger);
        }

        #endregion


        #region UserPreviewViewModel

        public UserPreviewViewModel UserPreviewViewModel => CreateUserPreviewViewModel();

        private UserPreviewViewModel CreateUserPreviewViewModel()
        {
            return new UserPreviewViewModel(_userRepository, _avatarRepository, _messenger);
        }

        #endregion

        #region CommentListViewModel

        public CommentListViewModel CommentListViewModel => CreateCommentListViewModel();

        private CommentListViewModel CreateCommentListViewModel()
        {
            return new CommentListViewModel(_commentRepository, _userRepository,
                _avatarRepository, _messenger);
        }

        #endregion

        #region TeamListViewModel

        public TeamListViewModel TeamListViewModel => CreateTeamListViewModel();

        private TeamListViewModel CreateTeamListViewModel()
        {
            return new TeamListViewModel(_teamRepository, _userRepository, _teamMemberRepository, _avatarRepository,
                _messenger);
        }

        #endregion


        #region PostListViewModel

        public PostListViewModel PostListViewModel => CreatePostListViewModel();

        private PostListViewModel CreatePostListViewModel()
        {
            return new PostListViewModel(_postRepository, _userRepository, _avatarRepository, _messenger);
        }

        #endregion

        #region HostPage

        public HostPageModel HostPageModel => CreateHostPageModel();

        private HostPageModel CreateHostPageModel()
        {
            return new HostPageModel();
        }

        #endregion

        #region LoginPageModel

        public LoginPageModel LoginPageModel => CreateLoginPageModel();

        private LoginPageModel CreateLoginPageModel()
        {
            return new LoginPageModel(_userRepository, _messenger);
        }

        #endregion

        #region RegistrationPageModel

        public RegistrationPageModel RegistrationPageModel => CreateRegistrationPageModel();

        private RegistrationPageModel CreateRegistrationPageModel()
        {
            return new RegistrationPageModel(_userRepository, _avatarRepository, _messenger);
        }

        #endregion

        #region HomePageModel

        public HomePageModel HomePageModel => CreateHomePageModel();

        private HomePageModel CreateHomePageModel()
        {
            return new HomePageModel(_messenger);
        }

        #endregion

        #region UserDetailPageModel

        public UserDetailPageModel UserDetailPageModel => CreateUserDetailPageModel();

        private UserDetailPageModel CreateUserDetailPageModel()
        {
            return new UserDetailPageModel(_userRepository, _teamRepository,
                _avatarRepository, _teamMemberRepository,
                _messenger);
        }

        #endregion

        #region TeamDetailPageModel

        public TeamDetailPageModel TeamDetailPageModel => CreateTeamDetailPageModel();

        private TeamDetailPageModel CreateTeamDetailPageModel()
        {
            return new TeamDetailPageModel(_teamRepository, _userRepository,
                _teamMemberRepository, _avatarRepository,
                _messenger);
        }

        #endregion

        #region TeamCreationPageModel

        public TeamCreationPageModel TeamCreationPageModel => CreateTeamCreationPageModel();

        private TeamCreationPageModel CreateTeamCreationPageModel()
        {
            return new TeamCreationPageModel(_teamRepository, _userRepository,
                _teamMemberRepository, _avatarRepository,
                _messenger);
        }

        #endregion
    }
}