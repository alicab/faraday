﻿using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;

namespace Faraday.APP.Services
{
    public class SessionService
    {
        private Messenger Messenger { get; } = Messenger.Instance;

        public UserModel User { get; set; }

        public string RegistrationLogin { get; set; }

        public SessionService()
        {
            Registration();
        }

        private void Registration()
        {
            Messenger.Register<UserMessage.Login>(message => User = message.User);
            Messenger.Register<UserMessage.Logout>(message =>
            {
                RegistrationLogin = User.Login;
                User = null;
                Messenger.Send(new PageMessage.GoTo {Page = AppPage.Login});
            });
            Messenger.Register<UserMessage.Request>(message => Messenger.Send(new UserMessage.Response {User = User}));
            Messenger.Register<RegistrationMessage.Login.Save>(message => RegistrationLogin = message.NewLogin);
            Messenger.Register<RegistrationMessage.Login.Request>(message => Messenger.Send(new RegistrationMessage.Login.Response {NewLogin = RegistrationLogin}));
        }
    }
}