﻿using System;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;

namespace Faraday.APP.Services
{
    public class GoToService
    {
        private Messenger Messenger { get; } = Messenger.Instance;

        private ViewModelLocator Locator { get; }

        public GoToService(ViewModelLocator locator)
        {
            Locator = locator;

            Registration();
        }

        private void Registration()
        {
            Messenger.Register<PageMessage.GoTo>(GotoPage);
        }

        private void GotoPage(PageMessage.GoTo message)
        {
            ViewModelBase currentPage;
            switch (message.Page)
            {
                case AppPage.Login:
                    currentPage = Locator.LoginPageModel;
                    break;
                case AppPage.Register:
                    currentPage = Locator.RegistrationPageModel;
                    break;
                case AppPage.HomeView:
                    currentPage = Locator.HomePageModel;
                    break;
                case AppPage.UserDetail:
                    currentPage = Locator.UserDetailPageModel;
                    break;
                case AppPage.TeamDetail:
                    currentPage = Locator.TeamDetailPageModel;
                    break;
                case AppPage.TeamCreation:
                    currentPage = Locator.TeamCreationPageModel;
                    break;
                default:
                    throw new NotImplementedException();
            }

            Messenger.Send(new PageMessage.Next { Page = currentPage });
        }
    }
}