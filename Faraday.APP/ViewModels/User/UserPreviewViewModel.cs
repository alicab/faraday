﻿using System.Windows.Input;
using Faraday.APP.Utilities.Command;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;
using Faraday.BL.Repository;

namespace Faraday.APP.ViewModels.User
{
    public class UserPreviewViewModel : ViewModelBase
    {
        public UserPreviewViewModel(UserRepository userRepository, AvatarRepository avatarRepository,
            Messenger messenger)
        {
            _userRepository = userRepository;
            _avatarRepository = avatarRepository;

            _messenger = messenger;
            _messenger.Register<UpdateDataMessage>(UpdateData);
            _messenger.Register<UserMessage.Login>(Login);

            GotoUserDetailCommand = new RelayCommand(GotoUserDetail);
            RefreshCommand = new RelayCommand(Refresh);
            LogoutCommand = new RelayCommand(Logout);
        }

        private readonly Messenger _messenger;

        #region Repositories

        private readonly UserRepository _userRepository;
        private readonly AvatarRepository _avatarRepository;

        #endregion

        #region Properties

        private byte[] _avatar;

        private UserModel _user;
        private string _name = "default";

        #endregion

        #region Properties Get Set

        public byte[] Avatar
        {
            get => _avatar;
            set
            {
                _avatar = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        private UserModel User
        {
            get => _user;
            set
            {
                _user = value;
                _name = _user.Name;
            }
        }

        #endregion

        #region ICommands

        public ICommand GotoUserDetailCommand { get; set; }
        public ICommand RefreshCommand { get; set; }
        public ICommand LogoutCommand { get; set; }

        #endregion

        #region Command Functions

        private void GotoUserDetail()
        {
            _messenger.Send(new PageMessage.GoTo {Page = AppPage.UserDetail});
        }

        private void Refresh()
        {
            _messenger.Send( new UpdateDataMessage());
        }

        private void Logout()
        {
            _messenger.Send(new UserMessage.Logout());
        }

        #endregion

        #region Messages

        private void Login(UserMessage.Login message)
        {
            _user = message.User;
            Avatar = _avatarRepository.GetById(message.User.PictureId ?? 0).ProfilePicture;
            Name = _user.Name;
        }

        private void UpdateData(UpdateDataMessage message)
        {
            _user = _userRepository.GetById(_user.Id);
            Avatar = _avatarRepository.GetById(_user.PictureId ?? 0).ProfilePicture;
            Name = _user.Name;
        }

        #endregion
    }
}