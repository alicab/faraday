﻿using System.Windows;
using System.Windows.Input;
using Faraday.APP.Utilities.Command;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;

namespace Faraday.APP.ViewModels.User
{
    public class UserListItemViewModel : ViewModelBase
    {
        public UserListItemViewModel(UserModel user, AvatarModel avatar, Messenger messenger, bool isMember,
            bool isLeader)
        {
            _messenger = messenger;

            ItemSelectedCommand = new RelayCommand(ItemSelected);

            _user = user;
            IsMember = isMember;
            IsLeader = isLeader ? Visibility.Visible : Visibility.Collapsed;
            UserId = user.Id;
            Name = user.Name;
            Avatar = avatar.ProfilePicture;
        }

        private readonly Messenger _messenger;
        private UserModel _user;

        #region Properties Get

        public int UserId { get; set; }
        public bool IsMember { get; set; }
        public byte[] Avatar { get; set; }
        public string Name { get; set; }
        public Visibility IsLeader { get; set; }
        public bool HasChangedState { get; set; }

        #endregion

        #region ICommands

        public ICommand ItemSelectedCommand { get; set; }

        #endregion

        #region Command functions

        private void ItemSelected()
        {
            _messenger.Send(new UserSelectedMessage {User = _user, IsMember = IsMember});
        }

        #endregion
    }
}