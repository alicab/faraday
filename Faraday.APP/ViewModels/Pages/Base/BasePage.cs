﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using Faraday.APP.Utilities.Animations;

namespace Faraday.APP.ViewModels.Pages.Base
{
    public class BasePage : UserControl
    {
        public BasePage()
        {
            if (DesignerProperties.GetIsInDesignMode(this))
                return;

            PageLoadAnimation = new PageAnimation
            {
                Slide = Slide.FromTop,
                Fade = Fade.In,
                AnimationDuration = 0.3f
            };
            PageUnloadAnimation = new PageAnimation
            {
                Slide = Slide.ToLeft,
                Fade = Fade.Out,
                AnimationDuration = 0.3f
            };
            //if (PageLoadAnimation.Slide != Slide.None)
            Visibility = Visibility.Visible;

            //Loaded += BasePage_Loaded;
        }

        public PageAnimation PageLoadAnimation;
        public PageAnimation PageUnloadAnimation;
        public static bool ShouldAnimateOut { get; set; }

        private async void BasePage_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (ShouldAnimateOut)
            {
                ShouldAnimateOut = false;
                await AnimateOut();
            }
            else
            {
                await AnimateIn();
                ShouldAnimateOut = true;
            }
        }

        private async Task AnimateIn()
        {
            await this.Animate(PageLoadAnimation);
        }

        private async Task AnimateOut()
        {
            await this.Animate(PageUnloadAnimation);
        }
    }
}