﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Input;
using Faraday.APP.Utilities.Command;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;
using Faraday.BL.Repository;
using Faraday.BL.Security;

namespace Faraday.APP.ViewModels.Pages
{
    public class LoginPageModel : ViewModelBase
    {
        public LoginPageModel(UserRepository userRepository, Messenger messenger)
        {
            _messenger = messenger;
            _userRepository = userRepository;

            _messenger.Register<RegistrationMessage.Login.Response>(message => Login = message.NewLogin);
            _messenger.Send(new RegistrationMessage.Login.Request());

            SignInCommand = new RelayCommand(async () => await SignIn());
            GotoRegisterCommand = new RelayCommand(GotoRegister);
        }

        private readonly Messenger _messenger;

        #region Repositories

        private readonly UserRepository _userRepository;

        #endregion

        #region Properties

        private string _login = "";
        private string _password = string.Empty;
        private string _loginStatus = string.Empty;
        private bool _isLogging;

        #endregion

        #region Properties Get Set

        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        public string LoginStatus
        {
            get => _loginStatus;
            set
            {
                _loginStatus = value;
                OnPropertyChanged();
            }
        }

        public bool IsLogging
        {
            get => _isLogging;
            set
            {
                _isLogging = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region ICommnads

        public ICommand SignInCommand { get; set; }
        public ICommand GotoRegisterCommand { get; set; }

        #endregion

        #region Command Functions

        private Task SignIn()
        {
            LoginStatus = string.Empty;
            return Task.Factory.StartNew(() =>
            {
                if (CheckInput())
                {
                    return;
                }

                SetPropertyValue(() => IsLogging, true);

                var user = FindUser();

                if (LoginStatus != string.Empty)
                {
                    SetPropertyValue(() => IsLogging, false);
                    return;
                }

                if (user is null || !PasswordProtection.IsPasswordValid(Password, user.Password))
                {
                    LoginStatus = "Invalid Login or Password!";
                    SetPropertyValue(() => IsLogging, false);
                    return;
                }

                _userRepository.LoadAvatar(user);

                SetPropertyValue(() => IsLogging, false);
                _messenger.Send(new UserMessage.Login {User = user});
                _messenger.Send(new PageMessage.GoTo {Page = AppPage.HomeView});
            });
        }

        private UserModel FindUser()
        {
            try
            {
                return _userRepository.GetByLogin(Login.ToLower());
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
                LoginStatus = "Connection problem";
                return null;
            }
        }

        private bool CheckInput()
        {
            if (Login == string.Empty)
            {
                LoginStatus = "Enter Login!";
                return true;
            }

            if (Password == string.Empty)
            {
                LoginStatus = "Enter Password!";
                return true;
            }

            return false;
        }

        private void GotoRegister()
        {
            _messenger.Send(new RegistrationMessage.Login.Save {NewLogin = Login});
            Login = string.Empty;
            Password = string.Empty;
            LoginStatus = string.Empty;
            _messenger.Send(new PageMessage.GoTo {Page = AppPage.Register});
        }

        #endregion
    }
}