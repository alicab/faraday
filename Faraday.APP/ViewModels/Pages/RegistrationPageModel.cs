﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Windows.Input;
using Faraday.APP.Utilities.Command;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;
using Faraday.BL.Repository;
using Faraday.BL.Security;

namespace Faraday.APP.ViewModels.Pages
{
    public class RegistrationPageModel : ViewModelBase
    {
        public RegistrationPageModel(UserRepository userRepository, AvatarRepository avatarRepository,
            Messenger messenger)
        {
            _userRepository = userRepository;
            _avatarRepository = avatarRepository;
            _messenger = messenger;

            _messenger.Register<RegistrationMessage.Login.Response>(message => Login = message.NewLogin);
            _messenger.Send(new RegistrationMessage.Login.Request());

            RegisterCommand = new RelayCommand(async () => await Register());
            CancelRegistrationCommand = new RelayCommand(CancelRegistration);
        }

        private readonly Messenger _messenger;

        #region Repositories

        private readonly UserRepository _userRepository;
        private readonly AvatarRepository _avatarRepository;

        #endregion

        #region Properties

        private string _password = string.Empty;
        private string _passwordControl = string.Empty;
        private string _registerStatus = string.Empty;
        private bool _isRegistering;

        #endregion

        #region Properties Get Set

        public string Login { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        public string PasswordControl
        {
            get => _passwordControl;
            set
            {
                _passwordControl = value;
                OnPropertyChanged();
            }
        }

        public string RegisterStatus
        {
            get => _registerStatus;
            set
            {
                _registerStatus = value;
                OnPropertyChanged();
            }
        }

        public bool IsRegistering
        {
            get => _isRegistering;
            set
            {
                _isRegistering = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region ICommnads

        public ICommand RegisterCommand { get; set; }
        public ICommand CancelRegistrationCommand { get; set; }

        #endregion

        #region Command Functions

        private Task Register()
        {
            return Task.Factory.StartNew(() =>
            {
                if (!CheckFields())
                {
                    return;
                }

                SetPropertyValue(() => IsRegistering, true);

                var newUser = RegisterUser();

                if (newUser is null)
                {
                    SetPropertyValue(() => IsRegistering, false);
                    return;
                }

                var bitmap = Properties.Resources.DEFAULT_USER;
                var image = new ImageConverter().ConvertTo(bitmap, typeof(byte[])) as byte[];

                var defaultAvatar = new AvatarModel
                {
                    ProfilePicture = image,
                    CreatedById = newUser.Id
                };
                var avatarModel = _avatarRepository.Insert(defaultAvatar);
                newUser.PictureId = avatarModel.Id;
                _userRepository.Update(newUser);


                SetPropertyValue(() => IsRegistering, false);

                _messenger.Send(new RegistrationMessage.Login.Save {NewLogin = Login});
                Email = string.Empty;
                Login = string.Empty;
                Name = string.Empty;
                Password = string.Empty;
                PasswordControl = string.Empty;
                RegisterStatus = string.Empty;
                _messenger.Send(new PageMessage.GoTo {Page = AppPage.Login});
            });
        }

        private UserModel RegisterUser()
        {
            try
            {
                if (_userRepository.LoginExists(Login.ToLower()))
                {
                    RegisterStatus = "Login is already in use";
                    SetPropertyValue(() => IsRegistering, false);
                    return null;
                }

                if (_userRepository.EmailExists(Email.ToLower()))
                {
                    RegisterStatus = "Email is already in use";
                    SetPropertyValue(() => IsRegistering, false);
                    return null;
                }

                return _userRepository.Insert(new UserModel
                {
                    Email = Email.ToLower(),
                    Login = Login.ToLower(),
                    Name = Name,
                    Password = PasswordProtection.Password2Hash(Password),
                });
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
                RegisterStatus = "Connection problem";
                return null;
            }
        }

        private bool CheckField(string value, int maxLength, string valueName)
        {
            if (value is null || value == string.Empty)
            {
                RegisterStatus = $"Enter your {valueName}";
                return false;
            }

            if (value.Length <= maxLength)
            {
                return true;
            }

            RegisterStatus = $"{valueName} is too long (max {maxLength} characters)";
            return false;
        }

        private string CheckEmail(string value)
        {
            if (value is null || value == string.Empty)
            {
                RegisterStatus = "Enter your Email address";
                return null;
            }

            try
            {
                var email = new MailAddress(value);
                return email.Address;
            }
            catch (Exception)
            {
                RegisterStatus = "Email address is invalid";
                return null;
            }
        }

        private bool CheckPassword()
        {
            if (Password is null || Password == string.Empty)
            {
                RegisterStatus = "Enter your password";
                PasswordControl = string.Empty;
                return false;
            }

            if (Password.Length < 6 || Password.Length > 128)
            {
                RegisterStatus = "Password must be between 6 - 128 characters long";
                PasswordControl = string.Empty;
                return false;
            }

            if (Password == PasswordControl)
            {
                return true;
            }

            RegisterStatus = "Passwords don't match";
            Password = string.Empty;
            PasswordControl = string.Empty;
            return false;
        }

        private bool CheckFields()
        {
            if (!CheckField(Login, 64, "Login") ||
                CheckEmail(Email) is null ||
                !CheckField(Name, 128, "Name"))
            {
                return false;
            }

            return CheckPassword();
        }

        private void CancelRegistration()
        {
            Email = string.Empty;
            Login = string.Empty;
            Name = string.Empty;
            Password = string.Empty;
            PasswordControl = string.Empty;
            RegisterStatus = string.Empty;
            _messenger.Send(new PageMessage.GoTo {Page = AppPage.Login});
        }

        #endregion
    }
}