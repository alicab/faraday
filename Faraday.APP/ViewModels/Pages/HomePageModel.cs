﻿using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;

namespace Faraday.APP.ViewModels.Pages
{
    public class HomePageModel : ViewModelBase
    {
        public HomePageModel(Messenger messenger)
        {
            _messenger = messenger;
        }

        private readonly Messenger _messenger;
    }
}