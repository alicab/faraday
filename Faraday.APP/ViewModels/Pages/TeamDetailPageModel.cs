﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Faraday.APP.Utilities.Command;
using Faraday.APP.Utilities.FileBrowser;
using Faraday.APP.ViewModels.Base;
using Faraday.APP.ViewModels.User;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;
using Faraday.BL.Repository;

namespace Faraday.APP.ViewModels.Pages
{
    public class TeamDetailPageModel : ViewModelBase
    {
        public TeamDetailPageModel(TeamRepository teamRepository, UserRepository userRepository,
            TeamMemberRepository teamMemberRepository, AvatarRepository avatarRepository, Messenger messenger)
        {
            _teamRepository = teamRepository;
            _avatarRepository = avatarRepository;
            _userRepository = userRepository;
            _teamMemberRepository = teamMemberRepository;

            _messenger = messenger;
            _messenger.Register<UserMessage.Response>(GetUser);
            _messenger.Register<SelectedTeamUpdateMessage.Response>(GetSelectedTeam);

            _messenger.Send(new UserMessage.Request());
            _messenger.Send(new SelectedTeamUpdateMessage.Request());
            _messenger.Register<UserSelectedMessage>(UserSelected);


            UpdateAvatarCommand = new RelayCommand(UpdateAvatar);
            RemoveTeamCommand = new RelayCommand(RemoveTeam);
            SaveChangesCommand = new RelayCommand(SaveChanges);
            GoToHomePageCommand = new RelayCommand(GoToHomePage);
        }

        private readonly Messenger _messenger;

        #region Repositories

        private readonly TeamRepository _teamRepository;
        private readonly AvatarRepository _avatarRepository;
        private readonly UserRepository _userRepository;
        private readonly TeamMemberRepository _teamMemberRepository;

        #endregion

        #region Properties

        private string _name;
        private string _founderName;
        private string _description;
        private string _errorString;
        private byte[] _avatar;

        private AvatarModel _originalAvatarModel;

        private Visibility _isTeamLeaderVisibility = Visibility.Hidden;
        private string _newName = string.Empty;

        private TeamModel _selectedTeam;
        private UserModel _user;

        private ObservableCollection<UserListItemViewModel> _members;
        private ObservableCollection<UserListItemViewModel> _users;

        #endregion

        #region Properties Get Set

        public string FounderName
        {
            get => _founderName;
            set
            {
                _founderName = value;
                OnPropertyChanged();
            }
        }

        public byte[] Avatar
        {
            get => _avatar;
            set
            {
                _avatar = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }

        public Visibility IsTeamLeaderVisibility
        {
            get => _isTeamLeaderVisibility;
            set
            {
                _isTeamLeaderVisibility = value;
                OnPropertyChanged();
            }
        }

        public string NewName
        {
            get => _newName;
            set
            {
                _newName = value;
                OnPropertyChanged();
            }
        }

        public string ErrorString
        {
            get => _errorString;
            set
            {
                _errorString = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<UserListItemViewModel> Members
        {
            get => _members;
            set
            {
                _members = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<UserListItemViewModel> Users
        {
            get => _users;
            set
            {
                _users = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region ICommand

        public ICommand RemoveTeamCommand { get; set; }
        public ICommand UpdateAvatarCommand { get; set; }
        public ICommand SaveChangesCommand { get; set; }
        public ICommand GoToHomePageCommand { get; set; }

        #endregion

        #region Command functions

        private void RemoveTeam()
        {
            if (_selectedTeam.CreatedById != _user.Id)
            {
                var member = _teamMemberRepository.GetById(_user.Id, _selectedTeam.Id);
                _teamMemberRepository.Remove(member);
            }
            else
            {
                _teamRepository.Remove(_selectedTeam.Id);
            }

            _messenger.Send(new UpdateDataMessage());
            _messenger.Send(new PageMessage.GoTo { Page = AppPage.HomeView });
        }

        private void UpdateAvatar()
        {
            var newAvatar = FileBrowserDialog.GetPicture();

            if (newAvatar == null) return;
            if (newAvatar == _originalAvatarModel.ProfilePicture) return;

            Avatar = newAvatar;
        }

        private void SaveChanges()
        {
            if (!((_selectedTeam.Name == NewName || NewName.Length == 0) &&
                  _selectedTeam.Description == Description))
            {
                _selectedTeam.Name = NewName.Length > 0 ? NewName : Name;
                _selectedTeam.Description = Description;

                _teamRepository.Update(_selectedTeam);
            }

            if (Avatar != _originalAvatarModel.ProfilePicture)
            {
                _originalAvatarModel.ProfilePicture = Avatar;
                _avatarRepository.Update(_originalAvatarModel);
            }

            Members.Where(m => m.HasChangedState)
                .ToList()
                .ForEach(m =>
                {
                    m.HasChangedState = false;
                    _teamMemberRepository.Insert(new TeamMemberModel
                        {
                            CreatedById = _user.Id,
                            MemberId = m.UserId,
                            TeamId = _selectedTeam.Id
                        });
                });

            Users.Where(u => u.HasChangedState)
                .ToList()
                .ForEach(u =>
                {
                    u.HasChangedState = false;
                    _teamMemberRepository.Remove(u.UserId, _selectedTeam.Id);
                });

            _messenger.Send(new UpdateDataMessage());
            _messenger.Send(new PageMessage.GoTo { Page = AppPage.HomeView });
        }

        private void GoToHomePage()
        {
            _messenger.Send(new LockTeamSelectMessage {IsTeamLocked = false});
            _messenger.Send(new PageMessage.GoTo {Page = AppPage.HomeView});
        }

        #endregion

        #region Messages

        private void GetUser(UserMessage.Response message)
        {
            _user = message.User;
        }

        private void GetSelectedTeam(SelectedTeamUpdateMessage.Response message)
        {
            _selectedTeam = message.SelectedTeam;
            if (message.SelectedTeam.CreatedById == _user.Id)
            {
                IsTeamLeaderVisibility = Visibility.Visible;
            }

            var avatarModel = _avatarRepository.GetById(_selectedTeam.PictureId ?? 0);
            if (avatarModel != null)
            {
                Avatar = avatarModel.ProfilePicture;
                _originalAvatarModel = avatarModel;
            }

            Name = _selectedTeam.Name;
            Description = _selectedTeam.Description;

            UpdateUsersAndMembers();
        }

        private void UserSelected(UserSelectedMessage message)
        {
            if (IsTeamLeaderVisibility == Visibility.Hidden)
            {
                return;
            }

            if (message.IsMember)
            {
                UserListItemViewModel memberViewModel = null;
                foreach (var member in Members)
                {
                    if (member.UserId == message.User.Id)
                    {
                        Members.Remove(member);
                        memberViewModel = member;
                        member.IsMember = false;
                        member.HasChangedState = !member.HasChangedState;
                        break;
                    }
                }

                Users.Add(memberViewModel);
            }
            else
            {
                UserListItemViewModel userViewModel = null;
                foreach (var user in Users)
                {
                    if (user.UserId == message.User.Id)
                    {
                        Users.Remove(user);
                        userViewModel = user;
                        user.IsMember = true;
                        user.HasChangedState = !user.HasChangedState;
                        break;
                    }
                }

                Members.Add(userViewModel);
            }
        }

        #endregion

        private void UpdateUsersAndMembers()
        {
            var newMembers = new ObservableCollection<UserListItemViewModel>();
            var newUsers = new ObservableCollection<UserListItemViewModel>();

            var allMembers = _teamMemberRepository.GetAllByTeamId(_selectedTeam.Id);
            var allUsers = _userRepository.GetAll();
            foreach (var user in allUsers)
            {
                var avatar = _avatarRepository.GetById(user.PictureId ?? 0);
                bool memberFound = false;
                foreach (var member in allMembers)
                {
                    if (member.MemberId == user.Id)
                    {
                        bool isLeader = _selectedTeam.CreatedById == user.Id;

                        if (_user.Id != user.Id)
                            newMembers.Add(new UserListItemViewModel(user, avatar, _messenger, true, isLeader));
                        memberFound = true;
                        break;
                    }
                }

                if (!memberFound)
                    newUsers.Add(new UserListItemViewModel(user, avatar, _messenger, false, false));
            }

            Users = newUsers;
            Members = newMembers;
        }
    }
}