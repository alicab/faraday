﻿using System.Drawing;
using System.Windows.Input;
using Faraday.APP.Utilities.Command;
using Faraday.APP.Utilities.FileBrowser;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;
using Faraday.BL.Repository;

namespace Faraday.APP.ViewModels.Pages
{
    public class TeamCreationPageModel : ViewModelBase
    {
        public TeamCreationPageModel(TeamRepository teamRepository, UserRepository userRepository,
            TeamMemberRepository teamMemberRepository, AvatarRepository avatarRepository, Messenger messenger)
        {
            _teamRepository = teamRepository;
            _avatarRepository = avatarRepository;
            _userRepository = userRepository;
            _teamMemberRepository = teamMemberRepository;

            _messenger = messenger;
            _messenger.Register<UserMessage.Response>(GetUser);
            _messenger.Send(new UserMessage.Request());

            var bitmap = Properties.Resources.DEFAULT_TEAM;
            Avatar = new ImageConverter().ConvertTo(bitmap, typeof(byte[])) as byte[];

            GoToHomePageCommand = new RelayCommand(GoToHomePage);
            CreateTeamCommand = new RelayCommand(CreateTeam);
            UpdateAvatarCommand = new RelayCommand(UpdateAvatar);
        }

        private readonly Messenger _messenger;

        #region Repositories

        private readonly TeamRepository _teamRepository;
        private readonly AvatarRepository _avatarRepository;
        private readonly UserRepository _userRepository;
        private readonly TeamMemberRepository _teamMemberRepository;

        #endregion

        #region Properties

        private UserModel _user;
        private byte[] _avatar;
        private string _name = string.Empty;
        private string _description = string.Empty;
        private string _errorString = string.Empty;

        #endregion

        #region Properties Get Set

        public byte[] Avatar
        {
            get => _avatar;
            set
            {
                _avatar = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }

        public string ErrorString
        {
            get => _errorString;
            set
            {
                _errorString = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region ICommand

        public ICommand GoToHomePageCommand { get; set; }
        public ICommand CreateTeamCommand { get; set; }
        public ICommand UpdateAvatarCommand { get; set; }

        #endregion

        #region Command functions

        private void GoToHomePage()
        {
            _messenger.Send(new PageMessage.GoTo {Page = AppPage.HomeView});
        }

        private void CreateTeam()
        {
            if (!ConfirmFields()) return;

            var avatarModel = new AvatarModel
            {
                CreatedById = _user.Id,
                ProfilePicture = Avatar
            };

            avatarModel = _avatarRepository.Insert(avatarModel);
            if (avatarModel == null) return;

            var newTeam = new TeamModel
            {
                CreatedById = _user.Id,
                Description = Description,
                Name = Name,
                PictureId = avatarModel.Id,
            };

            newTeam = _teamRepository.Insert(newTeam);
            if (newTeam == null) return;

            var newTeamMember = new TeamMemberModel
            {
                CreatedById = _user.Id,
                MemberId = _user.Id,
                TeamId = newTeam.Id
            };

            var member = _teamMemberRepository.Insert(newTeamMember);

            _messenger.Send(new TeamCreatedMessage {NewTeam = newTeam});
            _messenger.Send(new UpdateDataMessage());
        }

        private void UpdateAvatar()
        {
            var newAvatar = FileBrowserDialog.GetPicture();
            if (newAvatar == null) return;
            Avatar = newAvatar;
        }

        #endregion

        #region Messages

        private void GetUser(UserMessage.Response message)
        {
            _user = message.User;
        }

        #endregion

        private bool ConfirmFields()
        {
            if (Name.Length < 0 || Name.Length > 20)
            {
                ErrorString = "Invalid length of name";
                return false;
            }

            return true;
        }
    }
}