﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Faraday.APP.Utilities.Command;
using Faraday.APP.Utilities.FileBrowser;
using Faraday.APP.ViewModels.Base;
using Faraday.APP.ViewModels.Team;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;
using Faraday.BL.Repository;
using Faraday.BL.Security;

namespace Faraday.APP.ViewModels.Pages
{
    public class UserDetailPageModel : ViewModelBase
    {
        public UserDetailPageModel(UserRepository userRepository, TeamRepository teamRepository,
            AvatarRepository avatarRepository, TeamMemberRepository teamMemberRepository, Messenger messenger)
        {
            _userRepository = userRepository;
            _avatarRepository = avatarRepository;

            SaveChangesCommand = new RelayCommand(SaveChanges);
            ResetChangesCommand = new RelayCommand(ResetChanges);
            UpdateAvatarCommand = new RelayCommand(UpdateAvatar);
            GoToHomePageCommand = new RelayCommand(GoToHomePage);

            _messenger = messenger;
            _messenger.Register<UserMessage.Response>(GetUser);
            _messenger.Send(new UserMessage.Request());
        }

        private readonly Messenger _messenger;

        #region Repositories

        private readonly UserRepository _userRepository;
        private readonly AvatarRepository _avatarRepository;

        #endregion

        #region Properties

        private byte[] _avatar;
        private string _name = string.Empty;
        private string _email = string.Empty;
        private string _login = string.Empty;
        private string _newName = string.Empty;
        private string _password = string.Empty;
        private string _passwordControl = string.Empty;
        private string _errorString = string.Empty;

        private UserModel _user;
        private AvatarModel _originalAvatar;

        #endregion

        #region Properties Get Set

        public string Email
        {
            get => _email;
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }

        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public string NewName
        {
            get => _newName;
            set
            {
                _newName = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        public string PasswordControl
        {
            get => _passwordControl;
            set
            {
                _passwordControl = value;
                OnPropertyChanged();
            }
        }

        public byte[] Avatar
        {
            get => _avatar;
            set
            {
                _avatar = value;
                OnPropertyChanged();
            }
        }

        public string ErrorString
        {
            get => _errorString;
            set
            {
                _errorString = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region ICommands

        public ICommand SaveChangesCommand { get; set; }
        public ICommand ResetChangesCommand { get; set; }
        public ICommand UpdateAvatarCommand { get; set; }
        public ICommand GoToHomePageCommand { get; set; }

        #endregion

        #region Command Functions

        private void SaveChanges()
        {
            if (NewName == string.Empty &&
                _originalAvatar.ProfilePicture == Avatar &&
                Password == string.Empty &&
                PasswordControl == string.Empty)
                return;

            if (!ConfirmFields()) return;

            if (NewName.Length > 0)
            {
                Name = NewName;
                _user.Name = NewName;
            }

            if (Password.Length > 0)
                _user.Password = PasswordProtection.Password2Hash(Password);

            if (_originalAvatar != null && Avatar != _originalAvatar.ProfilePicture)
            {
                _originalAvatar.ProfilePicture = Avatar;
                _avatarRepository.Update(_originalAvatar);
            }

            _userRepository.Update(_user);

            NewName = string.Empty;
            Password = string.Empty;
            PasswordControl = string.Empty;

            _messenger.Send(new UpdateDataMessage());
        }

        private void ResetChanges()
        {
            Name = _user.Name;
            Avatar = _originalAvatar.ProfilePicture;
        }

        private void UpdateAvatar()
        {
            var newAvatar = FileBrowserDialog.GetPicture();
            if (newAvatar == null) return;
            if (_originalAvatar != null && _originalAvatar.ProfilePicture == newAvatar) return;

            Avatar = newAvatar;
        }

        private void GoToHomePage()
        {
            _messenger.Send(new PageMessage.GoTo {Page = AppPage.HomeView});
        }

        #endregion

        #region Functions

        private void GetUser(UserMessage.Response message)
        {
            _user = message.User;
            if (_user == null) return;
            Email = _user.Email;
            Login = _user.Login;
            Name = _user.Name;

            _originalAvatar = _avatarRepository.GetById(_user.PictureId ?? 0);
            Avatar = _originalAvatar.ProfilePicture;
        }

        #endregion

        private bool ConfirmFields()
        {
            if (Name.Length > 30)
            {
                ErrorString = "Name is too long";
                return false;
            }

            if (Password.Length < 6 && Password.Length > 0)
            {
                ErrorString = "Password must be at least 6 characters long";
                Password = string.Empty;
                PasswordControl = string.Empty;
                return false;
            }

            if ((Password.Length > 0 || PasswordControl.Length > 0) && Password != PasswordControl)
            {
                ErrorString = "Password does not match";
                Password = string.Empty;
                PasswordControl = string.Empty;
                return false;
            }


            return true;
        }
    }
}