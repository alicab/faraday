﻿using System.Windows.Input;
using Castle.Core.Internal;
using Faraday.APP.Utilities.Command;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;

namespace Faraday.APP.ViewModels.Team
{
    public class TeamListItemViewModel : ViewModelBase
    {
        public TeamListItemViewModel(TeamModel team, byte[] avatar, Messenger messenger)
        {
            Team = team;
            Name = team.Name;
            Avatar = avatar;

            _messenger = messenger;

            ItemSelectedCommand = new RelayCommand(ItemSelected);
            ItemDetailCommand = new RelayCommand(ItemDetail);
        }

        private readonly Messenger _messenger;

        #region Properties

        private bool _isSelcted;

        #endregion

        #region Properties Get Set

        public TeamModel Team { get; set; }
        public bool IsItemLocked { get; set; }


        public byte[] Avatar { get; set; }

        public bool IsSelected
        {
            get => _isSelcted;
            set
            {
                _isSelcted = value;
                OnPropertyChanged();
            }
        }

        public string Name { get; set; }

        #endregion

        #region ICommand

        public ICommand ItemSelectedCommand { get; set; }
        public ICommand ItemDetailCommand { get; set; }

        #endregion

        #region Command Functions

        private void ItemSelected()
        {
            if (IsSelected || IsItemLocked) return;
            IsSelected = true;
            _messenger.Send(new SelectedTeamUpdateMessage.Update {SelectedTeam = Team});
        }

        private void ItemDetail()
        {
            if (!IsSelected)
            {
                IsSelected = true;
                _messenger.Send(new SelectedTeamUpdateMessage.Update {SelectedTeam = Team});
            }

            _messenger.Send(new PageMessage.GoTo {Page = AppPage.TeamDetail});
            _messenger.Send(new LockTeamSelectMessage {IsTeamLocked = true});
        }

        #endregion

        #region Messages

        #endregion
    }
}