﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Castle.Core.Internal;
using Faraday.APP.Utilities.Command;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;
using Faraday.BL.Repository;

namespace Faraday.APP.ViewModels.Team
{
    public class TeamListViewModel : ViewModelBase
    {
        public TeamListViewModel(TeamRepository teamRepository, UserRepository userRepository,
            TeamMemberRepository teamMemberRepository, AvatarRepository avatarRepository, Messenger messenger)
        {
            _teamRepository = teamRepository;
            _teamMemberRepository = teamMemberRepository;
            _avatarRepository = avatarRepository;
            _userRepository = userRepository;

            GotoTeamDetailCommand = new RelayCommand(GotoTeamDetail);

            _messenger = messenger;
            _messenger.Register<SelectedTeamUpdateMessage.Update>(SelectedTeamUpdate);
            _messenger.Register<SelectedTeamUpdateMessage.Request>(GetSelectedTeam);
            _messenger.Register<UserMessage.Login>(Login);
            _messenger.Register<UpdateDataMessage>(UpdateData);
            _messenger.Register<TeamCreatedMessage>(TeamCreated);
            _messenger.Register<LockTeamSelectMessage>(LockTeamSelect);
        }

        private readonly Messenger _messenger;

        #region Repositories

        private readonly TeamRepository _teamRepository;
        private readonly TeamMemberRepository _teamMemberRepository;
        private readonly AvatarRepository _avatarRepository;
        private readonly UserRepository _userRepository;

        #endregion

        #region Properties

        private TeamListItemViewModel _selectedTeamVm = null;

        private List<TeamListItemViewModel> _teams
            = new List<TeamListItemViewModel>();

        private UserModel _user;

        #endregion

        #region ICommand

        public ICommand GotoTeamDetailCommand { get; set; }

        #endregion

        #region Command Functions

        private void GotoTeamDetail()
        {
            _messenger.Send(new PageMessage.GoTo {Page = AppPage.TeamDetail});
        }

        #endregion

        #region Properties Get Set

        public TeamListItemViewModel SelectedTeamVM
        {
            get => _selectedTeamVm;
            set
            {
                _selectedTeamVm = value;
                OnPropertyChanged();
            }
        }

        public List<TeamListItemViewModel> Teams
        {
            get => _teams;
            set
            {
                _teams = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Messages

        private void Login(UserMessage.Login message)
        {
            _user = message.User;
            UpdateTeams();
        }

        private void UpdateData(UpdateDataMessage message)
        {
            UpdateTeams();
        }

        private void TeamCreated(TeamCreatedMessage message)
        {
            var avatar = _avatarRepository.GetById(message.NewTeam.PictureId ?? 0).ProfilePicture;
            Teams.Add(new TeamListItemViewModel(message.NewTeam, avatar, _messenger));
            Teams = Teams;
            SelectedTeamVM = Teams.Last();
        }

        private void SelectedTeamUpdate(SelectedTeamUpdateMessage.Update message)
        {
            SelectedTeamVM.IsSelected = false;
            foreach (var team in _teams)
            {
                if (team.Team.Id == message.SelectedTeam.Id)
                {
                    SelectedTeamVM = team;
                    _messenger.Send(new SelectedTeamUpdateMessage.Response {SelectedTeam = SelectedTeamVM.Team});
                    return;
                }
            }
        }

        private void GetSelectedTeam(SelectedTeamUpdateMessage.Request message)
        {
            if (SelectedTeamVM == null) return;
            _messenger.Send(new SelectedTeamUpdateMessage.Response {SelectedTeam = SelectedTeamVM.Team});
        }


        private void LockTeamSelect(LockTeamSelectMessage message)
        {
            foreach (var team in Teams)
            {
                team.IsItemLocked = message.IsTeamLocked;
            }
        }

        #endregion

        #region Functions

        private void UpdateTeams()
        {
            var allTeamMembers = _teamMemberRepository.GetAllByUserId(_user.Id);
            List<TeamListItemViewModel> newList = new List<TeamListItemViewModel>();
            foreach (var teamMember in allTeamMembers)
            {
                var team = _teamRepository.GetById(teamMember.TeamId);
                var avatar = _avatarRepository.GetById(team.PictureId ?? 0).ProfilePicture;
                newList.Add(new TeamListItemViewModel(team, avatar, _messenger));
            }

            if (newList.Count == 0)
            {
                Teams = newList;
                SelectedTeamVM = null;
                return;
            }

            if (SelectedTeamVM != null)
            {
                var selectedTeamId = SelectedTeamVM.Team.Id;
                SelectedTeamVM = null;
                foreach (var teamModel in newList)
                {
                    if (teamModel.Team.Id == selectedTeamId)
                    {
                        SelectedTeamVM = teamModel;
                        SelectedTeamVM.IsSelected = true;
                    }
                }
            }

            Teams = newList;

            if (SelectedTeamVM == null)
            {
                SelectedTeamVM = Teams.First();
            }

            SelectedTeamVM.IsSelected = true;
        }

        #endregion
    }
}