﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Faraday.APP.Services;
using Faraday.APP.Utilities.Command;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;

namespace Faraday.APP.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel(Messenger messenger)
        {
            _messenger = messenger;
            _goToService = new GoToService(_viewModelLocator);

            _messenger.Register<PageMessage.Next>(message => CurrentPage = message.Page as ViewModelBase);
            _messenger.Register<UserMessage.Login>(message => SideBarWidth = 175);
            _messenger.Register<UserMessage.Logout>(message => SideBarWidth = 0);

            MinimizeWindowCommand = new RelayCommand<Window>(MinimizeWindow);
            MaximizeWindowCommand = new RelayCommand<Window>(MaximizeWindow);
            CloseWindowCommand = new RelayCommand<Window>(CloseWindow);
            GoToTeamCreationCommand = new RelayCommand(GoToTeamCreation);
        }

        private readonly Messenger _messenger;
        private static readonly ViewModelLocator _viewModelLocator = new ViewModelLocator();
        private GoToService _goToService;
        private SessionService _sessionService = new SessionService();


        #region Properties

        private ViewModelBase _currentPage = _viewModelLocator.LoginPageModel;
        private int _sideBarWidth = 0;

        #endregion

        #region Properties Get Set

        public ViewModelBase CurrentPage
        {
            get => _currentPage;
            private set
            {
                _currentPage = value;
                OnPropertyChanged();
            }
        }

        public int SideBarWidth
        {
            get => _sideBarWidth;
            set
            {
                _sideBarWidth = value;
                OnPropertyChanged();
            }
        }

        public bool IsSideBarVisible
        {
            get => SideBarWidth != 0;
            set => SideBarWidth = (value ? 175 : 0);
        }

        #endregion

        #region Messages

        #endregion

        public Task SlideSideMenu(bool state)
        {
            return Task.Run(async () =>
            {
                if (state)
                {
                    for (int i = 0; i < 175; i++)
                    {
                        SetPropertyValue(() => SideBarWidth, i);
                        await Task.Delay(800 / 175);
                    }
                }
                else
                {
                    for (int i = 175; i > 0; i--)
                    {
                        SetPropertyValue(() => SideBarWidth, i);
                        await Task.Delay(800 / 175);
                    }
                }
            });
        }


        #region ICommnads

        public ICommand MinimizeWindowCommand { get; }
        public ICommand MaximizeWindowCommand { get; }
        public ICommand CloseWindowCommand { get; }
        public ICommand GoToTeamCreationCommand { get; set; }

        #endregion

        #region Command Functions

        private void MinimizeWindow(Window window)
        {
            if (window != null)
            {
                window.WindowState = WindowState.Minimized;
            }
        }

        private void MaximizeWindow(Window window)
        {
            if (window != null)
            {
                window.WindowState = window.WindowState == WindowState.Maximized
                    ? WindowState.Normal
                    : WindowState.Maximized;
            }
        }

        private void CloseWindow(Window window)
        {
            window?.Close();
        }

        private void GoToTeamCreation()
        {
            _messenger.Send(new PageMessage.GoTo {Page = AppPage.TeamCreation});
        }

        #endregion
    }
}