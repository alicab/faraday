﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Faraday.APP.Utilities.Command;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;
using Faraday.BL.Repository;

namespace Faraday.APP.ViewModels.Comment
{
    public class CommentListViewModel : ViewModelBase
    {
        public CommentListViewModel(CommentRepository commentRepository, UserRepository userRepository,
            AvatarRepository avatarRepository, Messenger messenger)
        {
            _commentRepository = commentRepository;
            _avatarRepository = avatarRepository;
            _userRepository = userRepository;

            _messenger = messenger;
            _messenger.Register<SelectedPostUpdateMessage>(SelectedPostUpdate);
            _messenger.Register<SelectedTeamUpdateMessage.Response>(SelectedTeamUpdate);
            _messenger.Register<UserMessage.Response>(message => _user = message.User);
            _messenger.Register<UpdateDataMessage>(message => UpdateComments());

            _messenger.Send(new UserMessage.Request());

            AddCommentCommand = new RelayCommand(AddComment);
        }

        private readonly Messenger _messenger;

        #region Repositories

        private readonly UserRepository _userRepository;
        private readonly CommentRepository _commentRepository;
        private readonly AvatarRepository _avatarRepository;

        #endregion

        #region Properties

        private string _newBody;
        private UserModel _user;
        private PostModel _post;
        private TeamModel _team;
        private string _title;


        private List<CommentListItemViewModel> _comments
            = new List<CommentListItemViewModel>();

        private bool _scrollToBottom;
        private bool _isPostSelected = true;

        #endregion

        #region Properties Get Set

        public string NewBody
        {
            get => _newBody;
            set
            {
                _newBody = value;
                OnPropertyChanged();
            }
        }

        public UserModel User
        {
            get => _user;
            set
            {
                _user = value;
                OnPropertyChanged();
            }
        }

        public List<CommentListItemViewModel> Comments
        {
            get => _comments;
            set
            {
                _comments = value;
                OnPropertyChanged();
            }
        }

        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }

        public bool ScrollToBottom
        {
            get => _scrollToBottom;
            set
            {
                _scrollToBottom = value;
                OnPropertyChanged();
            }
        }

        public bool IsPostSelected
        {
            get => _isPostSelected;
            set
            {
                _isPostSelected = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region ICommand

        public ICommand AddCommentCommand { get; set; }

        #endregion

        #region Command funkce

        private void AddComment()
        {
            if (NewBody is null || NewBody == string.Empty)
            {
                return;
            }

            _commentRepository.Insert(new CommentModel
            {
                Body = NewBody,
                CreatedById = _user.Id,
                IsModified = false,
                ParentId = _post.Id
            });

            UpdateComments();
            Messenger.Instance.Send(new UpdateDataMessage()); // TODO hotfix

            NewBody = string.Empty;
        }

        #endregion

        #region Messages

        private void SelectedPostUpdate(SelectedPostUpdateMessage message)
        {
            if (message.SelectedPost == null)
            {
                IsPostSelected = true;
                Comments = null;
                Title = string.Empty;
                NewBody = string.Empty;
            }
            else
            {
                IsPostSelected = false;
                _post = message.SelectedPost;
                UpdateComments();
                Title = _post.Title;
            }
        }

        private void SelectedTeamUpdate(SelectedTeamUpdateMessage.Response message)
        {
            _team = message.SelectedTeam;
            Comments = null;
            _post = null;
            Title = string.Empty;
            IsPostSelected = true;
        }

        #endregion

        private void UpdateComments()
        {
            Comments = null;
            if (_post is null)
            {
                return;
            }

            var allComments = _commentRepository.GetAllByParent(_post.Id);
            var newList = (
                from comment in allComments
                let author = _userRepository.GetById(comment.CreatedById ?? 0)
                let avatar = _avatarRepository.GetById(author?.Id ?? 0)
                select new CommentListItemViewModel(comment, avatar ?? new AvatarModel(), _user)
            ).ToList();

            Comments = newList;
            ScrollToBottom = true;
        }
    }
}