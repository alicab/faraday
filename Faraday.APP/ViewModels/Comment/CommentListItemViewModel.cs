﻿using System;
using System.Drawing.Printing;
using System.Windows;
using Faraday.APP.Utilities.DateConverter;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Model;

namespace Faraday.APP.ViewModels.Comment
{
    public class CommentListItemViewModel : ViewModelBase
    {
        public CommentListItemViewModel(CommentModel comment, AvatarModel avatar, UserModel user)
        {
            Body = comment.Body;
            CreatedAt = DateConverter.DateToAge(comment.CreatedAt);
            CreatedBy = comment.CreatedBy.Name;

            if (avatar != null)
            {
                Avatar = avatar.ProfilePicture;
            }

            if (user.Id == comment.CreatedById)
            {
                return;
            }

            IsUserAuthor = false;
            IsNotUserAuthor = true;
            LeftColumnWidth = 30;
            RightColumnWidth = 0;
            CommentOffset = new Thickness {Left = 0, Bottom = 0, Right = 40, Top = 0};
            Alignment = HorizontalAlignment.Left;
        }

        private Thickness _commentOffset = new Thickness {Left = 40, Bottom = 0, Right = 0, Top = 0};

        #region Properties Get

        public string Body { get; set; }

        public string CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public byte[] Avatar { get; set; }
        public bool IsUserAuthor { get; set; } = true;
        public bool IsNotUserAuthor { get; set; } = false;
        public int LeftColumnWidth { get; set; } = 0;
        public int RightColumnWidth { get; set; } = 30;

        public Thickness CommentOffset
        {
            get => _commentOffset;
            set
            {
                _commentOffset = value;
                OnPropertyChanged();
            }
        }

        public HorizontalAlignment Alignment { get; set; } = HorizontalAlignment.Right;

        #endregion
    }
}