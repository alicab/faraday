﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;


namespace Faraday.APP.ViewModels.Base
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public void SetPropertyValue<T>(Expression<Func<T>> lambda, T val)
        {
            var property = (lambda as LambdaExpression).Body as MemberExpression;

            var propertyInfo = (PropertyInfo) property.Member;
            var target = Expression.Lambda(property.Expression).Compile().DynamicInvoke();

            propertyInfo.SetValue(target, val);
        }
    }
}