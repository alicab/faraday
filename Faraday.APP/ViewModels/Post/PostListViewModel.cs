﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Faraday.APP.Utilities.Command;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;
using Faraday.BL.Repository;

namespace Faraday.APP.ViewModels.Post
{
    public class PostListViewModel : ViewModelBase
    {
        public PostListViewModel(PostRepository postRepository, UserRepository userRepository,
            AvatarRepository avatarRepository, Messenger messenger)
        {
            _postRepository = postRepository;
            _userRepository = userRepository;
            _avatarRepository = avatarRepository;

            _messenger = messenger;

            _messenger.Register<SelectedTeamUpdateMessage.Response>(SelectedTeamUpdate);
            _messenger.Register<SelectedPostUpdateMessage>(message => SelectedPost = message.SelectedPost);
            _messenger.Register<UserMessage.Response>(message => _user = message.User);
            _messenger.Register<UpdateDataMessage>(message => Reload());

            _messenger.Send(new UserMessage.Request());
            _messenger.Send(new SelectedTeamUpdateMessage.Request());

            ReloadCommand = new RelayCommand(Reload);
            AddPostCommand = new RelayCommand(AddPost);
        }

        private readonly Messenger _messenger;

        #region Repositories

        private readonly PostRepository _postRepository;
        private readonly UserRepository _userRepository;
        private readonly AvatarRepository _avatarRepository;

        #endregion

        #region Properties

        private string _newTitle;
        private string _newBody;
        private UserModel _user;
        private TeamModel _team;
        private List<PostListItemViewModel> _posts;

        private bool _isTeamSelected = true;
        private bool _scrollToTop = false;

        #endregion

        #region Properties Get Set

        public string NewTitle
        {
            get => _newTitle;
            set
            {
                _newTitle = value;
                OnPropertyChanged();
            }
        }

        public string NewBody
        {
            get => _newBody;
            set
            {
                _newBody = value;
                OnPropertyChanged();
            }
        }

        public PostModel SelectedPost { get; set; }

        public List<PostListItemViewModel> Posts
        {
            get => _posts;
            set
            {
                _posts = value;
                OnPropertyChanged();
            }
        }

        public bool IsTeamSelected
        {
            get => _isTeamSelected;
            set
            {
                _isTeamSelected = value;
                OnPropertyChanged();
            }
        }

        public bool ScrollToTop
        {
            get => _scrollToTop;
            set
            {
                _scrollToTop = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region ICommand

        public ICommand ReloadCommand { get; set; }
        public ICommand AddPostCommand { get; set; }

        #endregion

        #region Command funkce

        private void Reload()
        {
            if (_team is null)
            {
                return;
            }

            Posts = null;
            var allTeamPosts = _postRepository
                .GetAllByTeam(_team.Id)
                .Select(p => {
                    _postRepository.LoadChildrenWithAuthor(p);
                    return p;
                })
                .OrderByDescending(p => p.Children
                    .OrderByDescending(c => c.CreatedAt)
                    .FirstOrDefault()?.CreatedAt
                )
                .ToList();

            var newPosts = (
                from teamPost in allTeamPosts
                let author = _userRepository.GetById(teamPost.CreatedById ?? 0)
                let avatar = _avatarRepository.GetById(author?.Id ?? 0)
                select new PostListItemViewModel(teamPost, avatar ?? new AvatarModel(), _messenger)
            ).ToList();

            Posts = newPosts;

            if (SelectedPost != null)
            {
                var post = Posts.FirstOrDefault(x => x.Id == SelectedPost.Id);
                if (post != null)
                {
                    _messenger.Send(new SelectedPostUpdateMessage {SelectedPost = post.Post});
                }
            }
        }

        private void AddPost()
        {
            if (!ConfirmFields()) return;

            var newPost = new PostModel
            {
                TeamId = _team.Id,
                CreatedById = _user.Id,
                Title = NewTitle,
                Body = NewBody ?? string.Empty
            };

            _postRepository.Insert(newPost);

            NewBody = string.Empty;
            NewTitle = string.Empty;
            Reload();
            if (_posts != null && _posts.Count > 0)
            {
                SelectedPost = Posts.First().Post;
                _messenger.Send(new SelectedPostUpdateMessage {SelectedPost = SelectedPost});
            }

            ScrollToTop = true;
        }

        #endregion

        #region Messages

        private void SelectedTeamUpdate(SelectedTeamUpdateMessage.Response message)
        {
            _team = message.SelectedTeam;
            IsTeamSelected = false;
            Reload();
        }

        #endregion

        #region Functions

        private bool ConfirmFields()
        {
            if (NewTitle.Length > 120)
            {
                NewTitle = string.Empty;
                return false;
            }

            return true;
        }

        #endregion
    }
}