﻿using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Faraday.APP.Utilities.Command;
using Faraday.APP.Utilities.DateConverter;
using Faraday.APP.ViewModels.Base;
using Faraday.BL.Messenger;
using Faraday.BL.Messenger.Messages;
using Faraday.BL.Model;

namespace Faraday.APP.ViewModels.Post
{
    public class PostListItemViewModel : ViewModelBase
    {
        public PostListItemViewModel(PostModel post, AvatarModel avatar, Messenger messenger)
        {
            Post = post;
            Id = post.Id;
            Title = post.Title;
            Body = post.Body;
            CreatedAt = DateConverter.DateToAge(post.CreatedAt);

            IsEmptyBody = post.Body is null || post.Body == string.Empty;
            if (IsEmptyBody)
            {
                _maxHeight = 0;
            }

            _messenger = messenger;

            if (post.CreatedBy != null)
            {
                CreatedBy = post.CreatedBy.Name;
            }

            if (avatar != null)
            {
                AuthorAvatar = avatar.ProfilePicture;
            }

            _messenger.Register<SelectedPostUpdateMessage>(SelectedPostUpdate);


            PostSelectedCommand = new RelayCommand(PostSelected);
        }

        private readonly Messenger _messenger;

        #region Properties

        private double _maxHeight = CUTT_OFF_HEIGHT;
        private Visibility _cutOffVisibility = Visibility.Visible;

        private const double CUTT_OFF_HEIGHT = 120f; // change in PostCutOffConverter is needed
        private const double MAX_HEIGHT = 2000f;

        #endregion


        #region Properties

        private bool _isSelected;
        private bool _isEmptyBody;

        #endregion

        #region Properties Get

        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public PostModel Post { get; set; }
        public string CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public byte[] AuthorAvatar { get; set; }

        public double MaxHeight
        {
            get => _maxHeight;
            set
            {
                _maxHeight = value;
                OnPropertyChanged();
            }
        }

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public bool IsEmptyBody
        {
            get => _isEmptyBody;
            set
            {
                _isEmptyBody = value;
                OnPropertyChanged();
            }
        }

        public Visibility CutOffVisibility
        {
            get => _cutOffVisibility;
            set
            {
                _cutOffVisibility = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region ICommand

        public ICommand PostSelectedCommand { get; set; }

        #endregion

        #region Command Functions

        private void PostSelected()
        {
            if (IsSelected)
            {
                MaxHeight = IsEmptyBody ? 0 : CUTT_OFF_HEIGHT;
                IsSelected = false;
                CutOffVisibility = Visibility.Visible;
                _messenger.Send(new SelectedPostUpdateMessage {SelectedPost = null});
            }
            else
            {
                MaxHeight = IsEmptyBody ? 0 : MAX_HEIGHT;
                IsSelected = true;
                CutOffVisibility = Visibility.Hidden;
                _messenger.Send(new SelectedPostUpdateMessage {SelectedPost = Post});
            }
        }

        #endregion

        #region Messages

        private void SelectedPostUpdate(SelectedPostUpdateMessage message)
        {
            if (message.SelectedPost == null)
            {
                return;
            }

            if (message.SelectedPost.Id == Id)
            {
                IsSelected = true;
                MaxHeight = IsEmptyBody ? 0 : MAX_HEIGHT;
                CutOffVisibility = Visibility.Hidden;
            }
            else
            {
                IsSelected = false;
                MaxHeight = IsEmptyBody ? 0 : CUTT_OFF_HEIGHT;
                CutOffVisibility = Visibility.Visible;
            }
        }

        #endregion

        #region Functions

        public Task AnimatePost(bool state)
        {
            return Task.Factory.StartNew(() =>
            {
                if (state)
                {
                    for (double i = CUTT_OFF_HEIGHT; i < 500; i++)
                    {
                        SetPropertyValue(() => MaxHeight, i);
                        Thread.Sleep((int) (100 / (500 - CUTT_OFF_HEIGHT)));
                    }

                    SetPropertyValue(() => MaxHeight, MAX_HEIGHT);
                }
                else
                {
                    for (double i = 500; i > CUTT_OFF_HEIGHT; i--)
                    {
                        SetPropertyValue(() => MaxHeight, i);
                        Thread.Sleep((int) (100 / (500 - CUTT_OFF_HEIGHT)));
                    }
                }
            });
        }

        #endregion
    }
}