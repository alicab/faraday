﻿namespace Faraday.APP.Utilities.Animations
{
    public enum LoadAnimation
    {
        None,
        SlideInFromRight,
        SlideInFromLeft,
        SlideInFromTop,
        SlideInFromBottom,
        SlideAndFadeInFromRight,
        SlideAndFadeInFromLeft,
        SlideAndFadeInFromTop,
        SlideAndFadeInFromBottom
    }

    public enum UnloadAnimation
    {
        None,
        SlideOutToRight,
        SlideOutToLeft,
        SlideOutToTop,
        SlideOutToBottom,
        SlideAndFadeOutToRight,
        SlideAndFadeOutToLeft,
        SlideAndFadeOutToTop,
        SlideAndFadeOutToBottom
    }

    public enum Slide
    {
        None,
        FromTop,
        ToTop,
        FromBottom,
        ToBottom,
        FromLeft,
        ToLeft,
        FromRight,
        ToRight
    }

    public enum Fade
    {
        None,
        In,
        Out
    }

    public class PageAnimation
    {
        public Slide Slide { get; set; }
        public Fade Fade { get; set; }
        public float AnimationDuration { get; set; }
    }
}