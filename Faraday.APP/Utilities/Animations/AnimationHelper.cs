﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace Faraday.APP.Utilities.Animations
{
    public static class AnimationHelper
    {
        public static async Task Animate(this UserControl userControl, PageAnimation pageAnimation)
        {
            var storyboard = new Storyboard();

            userControl.UpdateLayout();

            if (pageAnimation.Slide != Slide.None)
                storyboard.AddSlide(pageAnimation.Slide,
                    pageAnimation.AnimationDuration,
                    userControl.ActualWidth);

            if (pageAnimation.Fade != Fade.None)
                storyboard.AddFade(pageAnimation.AnimationDuration, pageAnimation.Fade);

            storyboard.Begin(userControl);

            userControl.Visibility = Visibility.Visible;

            await Task.Delay((int) (pageAnimation.AnimationDuration * 1000));
        }
    }
}