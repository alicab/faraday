﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Faraday.APP.Utilities.Animations
{
    public static class StoryboardHelper
    {
        public static void AddSlide(this Storyboard storyboard, Slide direction,
            float seconds, double offset, float deceleration = 0.9f)
        {
            Thickness from = new Thickness(0);
            Thickness to = new Thickness(0);

            switch (direction)
            {
                case Slide.FromTop:
                    from = new Thickness(0, -offset, 0, offset);
                    break;
                case Slide.FromBottom:
                    from = new Thickness(0, offset, 0, -offset);
                    break;
                case Slide.FromLeft:
                    from = new Thickness(-offset, 0, offset, 0);
                    break;
                case Slide.FromRight:
                    from = new Thickness(offset, 0, -offset, 0);
                    break;
                case Slide.ToTop:
                    to = new Thickness(0, -offset, 0, offset);
                    break;
                case Slide.ToBottom:
                    to = new Thickness(0, offset, 0, -offset);
                    break;
                case Slide.ToLeft:
                    to = new Thickness(-offset, 0, offset, 0);
                    break;
                case Slide.ToRight:
                    to = new Thickness(offset, 0, -offset, 0);
                    break;
            }

            var slideAnimation = new ThicknessAnimation
            {
                Duration = new Duration(TimeSpan.FromSeconds(seconds)),
                From = from,
                To = to,
                DecelerationRatio = deceleration
            };

            Storyboard.SetTargetProperty(slideAnimation, new PropertyPath("Margin"));
            storyboard.Children.Add(slideAnimation);
        }


        public static void AddFade(this Storyboard storyboard, float seconds, Fade fade)
        {
            int from = 1;
            int to = 1;

            switch (fade)
            {
                case Fade.In:
                    from = 0;
                    break;
                case Fade.Out:
                    to = 0;
                    break;
            }

            var fadeAnimation = new DoubleAnimation
            {
                Duration = new Duration(TimeSpan.FromSeconds(seconds)),
                From = from,
                To = to,
            };

            Storyboard.SetTargetProperty(fadeAnimation, new PropertyPath("Opacity"));
            storyboard.Children.Add(fadeAnimation);
        }
    }
}