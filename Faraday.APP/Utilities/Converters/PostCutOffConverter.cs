﻿using System;
using System.Globalization;

namespace Faraday.APP.Utilities.Converters
{
    public class PostCutOffConverter : BaseValueConverter<PostCutOffConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((double) value) > CutOff;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public double CutOff { get; set; } = 120f; // depends on value in PostListItemViewModel
    }
}