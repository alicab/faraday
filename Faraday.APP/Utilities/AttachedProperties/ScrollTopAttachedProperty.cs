﻿using System.Windows;
using System.Windows.Controls;

namespace Faraday.APP.Utilities.AttachedProperties
{
    public class ScrollTopAttachedProperty : BaseAttachedProperty<ScrollTopAttachedProperty, bool>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var scrollViewer = sender as ScrollViewer;
            if (scrollViewer == null) return;

            if ((bool) e.NewValue)
            {
                scrollViewer.ScrollToTop();
                SetValue(sender, false);
            }
        }
    }
}