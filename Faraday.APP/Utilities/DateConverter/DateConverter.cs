﻿using System;

namespace Faraday.APP.Utilities.DateConverter
{
    public static class DateConverter
    {
        public static string DateToAge(DateTime? date)
        {
            DateTime newDate;
            if (date != null)
            {
                newDate = (DateTime) date;
            }
            else
                return string.Empty;

            var age = DateTime.Now - date;
            if (age.Value.TotalMinutes < 1)
            {
                return age.Value.Seconds.ToString() + "seconds ago";
            }

            if (age.Value.TotalHours < 1)
            {
                return age.Value.Minutes.ToString() + " minutes ago";
            }

            if (age.Value.TotalDays < 1)
            {
                return age.Value.Hours.ToString() + " hours ago";
            }

            return newDate.ToString("d. M. yyyy");
        }
    }
}