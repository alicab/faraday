﻿using System.IO;
using Microsoft.Win32;

namespace Faraday.APP.Utilities.FileBrowser
{
    public static class FileBrowserDialog
    {
        public static byte[] GetPicture()
        {
            var openFileDialog = InitializeFileDialog();
            openFileDialog.ShowDialog();

            string fileName = openFileDialog.FileName;

            if (fileName == string.Empty) return null;

            var bytes = File.ReadAllBytes(fileName);

            if (bytes.Length == 0 || bytes.Length > 3000000)
                return null;
            return bytes;
        }

        private static OpenFileDialog InitializeFileDialog()
        {
            return new OpenFileDialog
            {
                Filter = "Images (*.jpg;*.png;)|*.jpg;*.png",

                Multiselect = false,
                Title = "Select new Avatar",
                CheckFileExists = true
            };
        }
    }
}