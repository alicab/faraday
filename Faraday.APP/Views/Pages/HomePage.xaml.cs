﻿using Faraday.APP.Utilities.Animations;
using Faraday.APP.ViewModels.Pages.Base;

namespace Faraday.APP.Views.Pages
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : BasePage
    {
        public HomePage()
        {
            PageLoadAnimation = new PageAnimation
            {
                Slide = Slide.FromTop,
                Fade = Fade.In,
                AnimationDuration = 1f
            };
            InitializeComponent();
        }
    }
}