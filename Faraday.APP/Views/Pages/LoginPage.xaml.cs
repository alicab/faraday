﻿using Faraday.APP.ViewModels.Pages.Base;
using System.Windows;
using System.Windows.Controls;

namespace Faraday.APP.Views.Pages
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : BasePage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            {
                ((dynamic) this.DataContext).Password = ((PasswordBox) sender).Password;
            }
        }
    }
}