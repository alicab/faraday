﻿using System.Windows;
using System.Windows.Controls;
using Faraday.APP.ViewModels.Base;
using Faraday.APP.ViewModels.Pages.Base;

namespace Faraday.APP.Views.Pages
{
    /// <summary>
    /// Interaction logic for HostPage.xaml
    /// </summary>
    public partial class HostPage : UserControl
    {
        public HostPage()
        {
            InitializeComponent();
        }

        public BasePage CurrentPage
        {
            get => (BasePage) GetValue(CurrentPageProperty);
            set => SetValue(CurrentPageProperty, value);
        }

        public static readonly DependencyProperty CurrentPageProperty =
            DependencyProperty.Register(nameof(CurrentPage), typeof(ViewModelBase), typeof(HostPage),
                new UIPropertyMetadata(propertyChangedCallback: CurrentPagePropertyChanged));

        private static void CurrentPagePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var newPageFrame = (d as HostPage).NewPage;
            var oldPageFrame = (d as HostPage).OldPage;

            var oldPageContent = newPageFrame.Content;

            newPageFrame.Content = null;

            oldPageFrame.Content = null; //oldPageContent;

            newPageFrame.Content = e.NewValue;
        }
    }
}