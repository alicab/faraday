﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Faraday.APP.Views.Team
{
    /// <summary>
    /// Interaction logic for TeamListItemView.xaml
    /// </summary>
    public partial class TeamListItemView : UserControl
    {
        public TeamListItemView()
        {
            InitializeComponent();
        }
    }
}