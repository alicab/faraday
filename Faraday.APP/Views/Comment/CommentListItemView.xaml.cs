﻿using System.Windows.Controls;

namespace Faraday.APP.Views.Comment
{
    /// <summary>
    /// Interaction logic for CommentListItemView.xaml
    /// </summary>
    public partial class CommentListItemView : UserControl
    {
        public CommentListItemView()
        {
            InitializeComponent();
        }
    }
}