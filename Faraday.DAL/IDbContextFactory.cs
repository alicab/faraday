﻿namespace Faraday.DAL
{
    public interface IDbContextFactory
    {
        AppDbContext CreateDbContext();
    }
}