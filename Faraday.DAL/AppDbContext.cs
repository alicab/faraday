using Microsoft.EntityFrameworkCore;
using Faraday.DAL.Entities;

namespace Faraday.DAL
{
    public class AppDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Avatar> Avatars { get; set; }

        public DbSet<Team> Teams { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<TeamMember> TeamMembers { get; set; }

        public DbSet<Activity> Activities { get; set; }

        public AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            TeamMemberModelCreating(modelBuilder);
            UserModelCreating(modelBuilder);
            TeamModelCreating(modelBuilder);
            CommentModelCreating(modelBuilder);
            PostModelCreating(modelBuilder);
            AvatarModelCreating(modelBuilder);
            ActivityModelCreating(modelBuilder);
        }

        private static void ActivityModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Activity>()
                .HasOne(a => a.CreatedBy)
                .WithMany(u => u.Activities)
                .HasForeignKey(k => k.CreatedById)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Activity>()
                .Property(x => x.CreatedAt)
                .HasDefaultValueSql("GetDate()");
            modelBuilder.Entity<Activity>()
                .Property(x => x.CreatedAt)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Activity>()
                .HasOne(a => a.Avatar)
                .WithMany(x => x.RecentActivities);
            //.HasForeignKey(k => k.AvatarId)
            //.OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Activity>()
                .HasOne(a => a.Post)
                .WithMany(x => x.RecentActivities);
            //.HasForeignKey(k => k.PostId)
            //.OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Activity>()
                .HasOne(a => a.Comment)
                .WithMany(x => x.RecentActivities);
            //.HasForeignKey(k => k.CommentId)
            //.OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Activity>()
                .HasOne(a => a.Team)
                .WithMany(x => x.RecentActivities);
            //.HasForeignKey(k => k.TeamId)
            //.OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Activity>()
                .HasOne(a => a.TeamMember)
                .WithMany(x => x.RecentActivities);
            //.HasForeignKey(k => new { k.TeamMemberId, k.UserMemberId })
            //.OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Activity>()
                .HasOne(a => a.User)
                .WithMany(x => x.RecentActivities);
            //.HasForeignKey(k => k.UserId)
            //.OnDelete(DeleteBehavior.Restrict);
        }

        private static void AvatarModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Avatar>()
                .HasOne(c => c.CreatedBy)
                .WithMany(u => u.Pictures)
                .HasForeignKey(k => k.CreatedById)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Avatar>()
                .Property(x => x.CreatedAt)
                .HasDefaultValueSql("GetDate()");
            modelBuilder.Entity<Avatar>()
                .Property(x => x.CreatedAt)
                .ValueGeneratedOnAdd();
        }

        private static void CommentModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>()
                .HasOne(c => c.Parent)
                .WithMany(c => c.Children)
                .HasForeignKey(k => k.ParentId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Comment>()
                .HasOne(c => c.CreatedBy)
                .WithMany(u => u.Comments)
                .HasForeignKey(k => k.CreatedById)
                .OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Comment>()
                .Property(x => x.CreatedAt)
                .HasDefaultValueSql("GetDate()");
            modelBuilder.Entity<Comment>()
                .Property(x => x.CreatedAt)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<Comment>()
                .Property(x => x.ModifiedAt)
                .HasDefaultValueSql("GetDate()");
            modelBuilder.Entity<Comment>()
                .Property(x => x.ModifiedAt)
                .ValueGeneratedOnUpdate();
        }

        private static void PostModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>()
                .HasOne(c => c.Team)
                .WithMany(t => t.Posts)
                .HasForeignKey(k => k.TeamId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Post>()
                .HasOne(c => c.CreatedBy)
                .WithMany(u => u.Posts)
                .HasForeignKey(k => k.CreatedById)
                .OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Post>()
                .Property(x => x.CreatedAt)
                .HasDefaultValueSql("GetDate()");
            modelBuilder.Entity<Post>()
                .Property(x => x.CreatedAt)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<Post>()
                .Property(x => x.ModifiedAt)
                .HasDefaultValueSql("GetDate()");
            modelBuilder.Entity<Post>()
                .Property(x => x.ModifiedAt)
                .ValueGeneratedOnUpdate();
        }

        private static void TeamModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>().HasIndex(t => t.Name);
            modelBuilder.Entity<Team>()
                .HasOne(t => t.CreatedBy)
                .WithMany(u => u.FounderOf)
                .HasForeignKey(t => t.CreatedById)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Team>()
                .HasOne(t => t.Picture)
                .WithOne(a => a.Team)
                .HasForeignKey<Team>(k => k.AvatarId)
                .OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Team>()
                .Property(x => x.CreatedAt)
                .HasDefaultValueSql("GetDate()");
            modelBuilder.Entity<Team>()
                .Property(x => x.CreatedAt)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<Team>()
                .Property(x => x.ModifiedAt)
                .HasDefaultValueSql("GetDate()");
            modelBuilder.Entity<Team>()
                .Property(x => x.ModifiedAt)
                .ValueGeneratedOnUpdate();
        }

        private static void UserModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasIndex(u => u.Email).IsUnique();
            modelBuilder.Entity<User>().HasIndex(u => u.Login).IsUnique();
            modelBuilder.Entity<User>()
                .HasOne(u => u.Picture)
                .WithOne(a => a.User)
                .HasForeignKey<User>(k => k.AvatarId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<User>()
                .Property(x => x.CreatedAt)
                .HasDefaultValueSql("GetDate()");
            modelBuilder.Entity<User>()
                .Property(x => x.CreatedAt)
                .ValueGeneratedOnAdd();
        }

        private static void TeamMemberModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TeamMember>()
                .HasKey(k => new {k.TeamId, k.MemberId});
            modelBuilder.Entity<TeamMember>().Ignore(x => x.Id);
            modelBuilder.Entity<TeamMember>()
                .HasOne(fk => fk.Team)
                .WithMany(t => t.Members)
                .HasForeignKey(k => k.TeamId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TeamMember>()
                .HasOne(fk => fk.Member)
                .WithMany(u => u.MemberOf)
                .HasForeignKey(k => k.MemberId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TeamMember>()
                .HasOne(fk => fk.CreatedBy)
                .WithMany(u => u.CreatedMembers)
                .HasForeignKey(k => k.CreatedById)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TeamMember>()
                .Property(x => x.CreatedAt)
                .HasDefaultValueSql("GetDate()");
            modelBuilder.Entity<TeamMember>()
                .Property(x => x.CreatedAt)
                .ValueGeneratedOnAdd();
        }
    }
}