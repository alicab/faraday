﻿// <auto-generated />
using System;
using Faraday.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Faraday.DAL.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20190503214516_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Faraday.DAL.Entities.Activity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ActivityType");

                    b.Property<int?>("AvatarId");

                    b.Property<int?>("CommentId");

                    b.Property<DateTime?>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("GetDate()");

                    b.Property<int?>("CreatedById");

                    b.Property<int?>("PostId");

                    b.Property<int?>("TeamId");

                    b.Property<int?>("TeamMemberId");

                    b.Property<int?>("TeamMemberMemberId");

                    b.Property<int?>("TeamMemberTeamId");

                    b.Property<int?>("UserId");

                    b.Property<int?>("UserMemberId");

                    b.HasKey("Id");

                    b.HasIndex("AvatarId");

                    b.HasIndex("CommentId");

                    b.HasIndex("CreatedById");

                    b.HasIndex("PostId");

                    b.HasIndex("TeamId");

                    b.HasIndex("UserId");

                    b.HasIndex("TeamMemberTeamId", "TeamMemberMemberId");

                    b.ToTable("Activities");
                });

            modelBuilder.Entity("Faraday.DAL.Entities.Avatar", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("GetDate()");

                    b.Property<int?>("CreatedById");

                    b.Property<byte[]>("ProfilePicture");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.ToTable("Avatars");
                });

            modelBuilder.Entity("Faraday.DAL.Entities.Comment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Body")
                        .HasMaxLength(4096);

                    b.Property<DateTime?>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("GetDate()");

                    b.Property<int?>("CreatedById");

                    b.Property<bool>("IsModified");

                    b.Property<DateTime?>("ModifiedAt")
                        .ValueGeneratedOnUpdate()
                        .HasDefaultValueSql("GetDate()");

                    b.Property<int?>("ParentId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("ParentId");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("Faraday.DAL.Entities.Post", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Body")
                        .HasMaxLength(4096);

                    b.Property<DateTime?>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("GetDate()");

                    b.Property<int?>("CreatedById");

                    b.Property<bool>("IsModified");

                    b.Property<DateTime?>("ModifiedAt")
                        .ValueGeneratedOnUpdate()
                        .HasDefaultValueSql("GetDate()");

                    b.Property<int?>("TeamId");

                    b.Property<string>("Title")
                        .HasMaxLength(512);

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("TeamId");

                    b.ToTable("Posts");
                });

            modelBuilder.Entity("Faraday.DAL.Entities.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AvatarId");

                    b.Property<DateTime?>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("GetDate()");

                    b.Property<int?>("CreatedById");

                    b.Property<string>("Description")
                        .HasMaxLength(4096);

                    b.Property<DateTime?>("ModifiedAt")
                        .ValueGeneratedOnUpdate()
                        .HasDefaultValueSql("GetDate()");

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("AvatarId")
                        .IsUnique()
                        .HasFilter("[AvatarId] IS NOT NULL");

                    b.HasIndex("CreatedById");

                    b.HasIndex("Name");

                    b.ToTable("Teams");
                });

            modelBuilder.Entity("Faraday.DAL.Entities.TeamMember", b =>
                {
                    b.Property<int>("TeamId");

                    b.Property<int>("MemberId");

                    b.Property<DateTime?>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("GetDate()");

                    b.Property<int?>("CreatedById");

                    b.HasKey("TeamId", "MemberId");

                    b.HasIndex("CreatedById");

                    b.HasIndex("MemberId");

                    b.ToTable("TeamMembers");
                });

            modelBuilder.Entity("Faraday.DAL.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AvatarId");

                    b.Property<DateTime?>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("GetDate()");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<string>("Login")
                        .HasMaxLength(64);

                    b.Property<string>("Name")
                        .HasMaxLength(128);

                    b.Property<string>("Password")
                        .HasMaxLength(128);

                    b.HasKey("Id");

                    b.HasIndex("AvatarId")
                        .IsUnique()
                        .HasFilter("[AvatarId] IS NOT NULL");

                    b.HasIndex("Email")
                        .IsUnique()
                        .HasFilter("[Email] IS NOT NULL");

                    b.HasIndex("Login")
                        .IsUnique()
                        .HasFilter("[Login] IS NOT NULL");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Faraday.DAL.Entities.Activity", b =>
                {
                    b.HasOne("Faraday.DAL.Entities.Avatar", "Avatar")
                        .WithMany("RecentActivities")
                        .HasForeignKey("AvatarId");

                    b.HasOne("Faraday.DAL.Entities.Comment", "Comment")
                        .WithMany("RecentActivities")
                        .HasForeignKey("CommentId");

                    b.HasOne("Faraday.DAL.Entities.User", "CreatedBy")
                        .WithMany("Activities")
                        .HasForeignKey("CreatedById")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Faraday.DAL.Entities.Post", "Post")
                        .WithMany("RecentActivities")
                        .HasForeignKey("PostId");

                    b.HasOne("Faraday.DAL.Entities.Team", "Team")
                        .WithMany("RecentActivities")
                        .HasForeignKey("TeamId");

                    b.HasOne("Faraday.DAL.Entities.User", "User")
                        .WithMany("RecentActivities")
                        .HasForeignKey("UserId");

                    b.HasOne("Faraday.DAL.Entities.TeamMember", "TeamMember")
                        .WithMany("RecentActivities")
                        .HasForeignKey("TeamMemberTeamId", "TeamMemberMemberId");
                });

            modelBuilder.Entity("Faraday.DAL.Entities.Avatar", b =>
                {
                    b.HasOne("Faraday.DAL.Entities.User", "CreatedBy")
                        .WithMany("Pictures")
                        .HasForeignKey("CreatedById")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Faraday.DAL.Entities.Comment", b =>
                {
                    b.HasOne("Faraday.DAL.Entities.User", "CreatedBy")
                        .WithMany("Comments")
                        .HasForeignKey("CreatedById")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Faraday.DAL.Entities.Post", "Parent")
                        .WithMany("Children")
                        .HasForeignKey("ParentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Faraday.DAL.Entities.Post", b =>
                {
                    b.HasOne("Faraday.DAL.Entities.User", "CreatedBy")
                        .WithMany("Posts")
                        .HasForeignKey("CreatedById")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Faraday.DAL.Entities.Team", "Team")
                        .WithMany("Posts")
                        .HasForeignKey("TeamId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Faraday.DAL.Entities.Team", b =>
                {
                    b.HasOne("Faraday.DAL.Entities.Avatar", "Picture")
                        .WithOne("Team")
                        .HasForeignKey("Faraday.DAL.Entities.Team", "AvatarId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("Faraday.DAL.Entities.User", "CreatedBy")
                        .WithMany("FounderOf")
                        .HasForeignKey("CreatedById")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Faraday.DAL.Entities.TeamMember", b =>
                {
                    b.HasOne("Faraday.DAL.Entities.User", "CreatedBy")
                        .WithMany("CreatedMembers")
                        .HasForeignKey("CreatedById")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Faraday.DAL.Entities.User", "Member")
                        .WithMany("MemberOf")
                        .HasForeignKey("MemberId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Faraday.DAL.Entities.Team", "Team")
                        .WithMany("Members")
                        .HasForeignKey("TeamId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Faraday.DAL.Entities.User", b =>
                {
                    b.HasOne("Faraday.DAL.Entities.Avatar", "Picture")
                        .WithOne("User")
                        .HasForeignKey("Faraday.DAL.Entities.User", "AvatarId")
                        .OnDelete(DeleteBehavior.Restrict);
                });
#pragma warning restore 612, 618
        }
    }
}
