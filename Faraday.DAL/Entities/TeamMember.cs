using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Faraday.DAL.Entities.Base;

namespace Faraday.DAL.Entities
{
    public class TeamMember : EntityId
    {
        public int TeamId { get; set; }

        [NotMapped] public Team Team { get; set; }

        public int MemberId { get; set; }

        [NotMapped] public User Member { get; set; }

        public int? CreatedById { get; set; }

        [NotMapped] public User CreatedBy { get; set; }

        public ICollection<Activity> RecentActivities { get; set; }
    }
}