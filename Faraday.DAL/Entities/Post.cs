﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Faraday.DAL.Entities
{
    public class Post : Contribution
    {
        [StringLength(512)] public string Title { get; set; }

        public int? TeamId { get; set; }

        [NotMapped] public Team Team { get; set; }

        public ICollection<Comment> Children { get; set; }
    }
}