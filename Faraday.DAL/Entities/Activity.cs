using System.ComponentModel.DataAnnotations.Schema;
using Faraday.DAL.Entities.Base;

namespace Faraday.DAL.Entities
{
    public class Activity : EntityBase
    {
        public int? TeamId { get; set; }

        [NotMapped] public Team Team { get; set; }

        public int? UserId { get; set; }

        [NotMapped] public User User { get; set; }

        public int? AvatarId { get; set; }

        [NotMapped] public Avatar Avatar { get; set; }

        public int? PostId { get; set; }

        [NotMapped] public Post Post { get; set; }

        public int? CommentId { get; set; }

        [NotMapped] public Comment Comment { get; set; }

        public int? TeamMemberId { get; set; }

        public int? UserMemberId { get; set; }

        [NotMapped] public TeamMember TeamMember { get; set; }

        public enum Type
        {
            Insert = 1,
            Update = 2,
            Delete = 3
        }

        public Type ActivityType { get; set; }
    }
}