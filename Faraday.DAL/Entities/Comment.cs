﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Faraday.DAL.Entities
{
    public class Comment : Contribution
    {
        public int? ParentId { get; set; }

        [NotMapped] public Post Parent { get; set; }
    }
}