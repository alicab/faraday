﻿using System;

namespace Faraday.DAL.Entities.Base
{
    public class EntityId
    {
        public int Id { get; set; }

        public DateTime? CreatedAt { get; set; }
    }
}