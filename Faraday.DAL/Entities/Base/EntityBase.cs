﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Faraday.DAL.Entities.Base
{
    public abstract class EntityBase : EntityId, IEntity
    {
        public int? CreatedById { get; set; }

        [NotMapped] public User CreatedBy { get; set; }
    }
}