﻿using System;

namespace Faraday.DAL.Entities.Base
{
    public interface IEntity
    {
        int Id { get; set; }
        DateTime? CreatedAt { get; set; }

        int? CreatedById { get; set; }

        User CreatedBy { get; set; }
    }
}