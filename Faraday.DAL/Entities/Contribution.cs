﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Faraday.DAL.Entities.Base;

namespace Faraday.DAL.Entities
{
    [NotMapped]
    public abstract class Contribution : EntityBase
    {
        [StringLength(4096)] public string Body { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public bool IsModified { get; set; }

        public ICollection<Activity> RecentActivities { get; set; }
    }
}