﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Faraday.DAL.Entities.Base;

namespace Faraday.DAL.Entities
{
    public class User : EntityId
    {
        /// <summary>
        /// http://www.rfc-editor.org/errata/eid1690
        /// </summary>
        [StringLength(256)]
        public string Email { get; set; }

        [StringLength(64)] public string Login { get; set; }

        [StringLength(128)] public string Name { get; set; }

        [StringLength(128)] public string Password { get; set; }

        public int? AvatarId { get; set; }

        [NotMapped] public Avatar Picture { get; set; }

        public ICollection<Post> Posts { get; set; }

        public ICollection<Comment> Comments { get; set; }

        public ICollection<TeamMember> MemberOf { get; set; }

        public ICollection<Team> FounderOf { get; set; }

        public ICollection<TeamMember> CreatedMembers { get; set; }

        public ICollection<Avatar> Pictures { get; set; }

        public ICollection<Activity> Activities { get; set; }

        public ICollection<Activity> RecentActivities { get; set; }
    }
}