﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Faraday.DAL.Entities.Base;

namespace Faraday.DAL.Entities
{
    public class Avatar : EntityBase
    {
        public byte[] ProfilePicture { get; set; }

        [NotMapped] public User User { get; set; }

        [NotMapped] public Team Team { get; set; }

        public ICollection<Activity> RecentActivities { get; set; }
    }
}