﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Faraday.DAL.Entities.Base;

namespace Faraday.DAL.Entities
{
    public class Team : EntityBase
    {
        [StringLength(256)] public string Name { get; set; }

        [StringLength(4096)] public string Description { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public int? AvatarId { get; set; }

        [NotMapped] public Avatar Picture { get; set; }

        public ICollection<TeamMember> Members { get; set; }

        public ICollection<Post> Posts { get; set; }

        public ICollection<Activity> RecentActivities { get; set; }
    }
}