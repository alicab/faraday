﻿using System;
using System.Linq;
using System.Reflection;
using Faraday.BL.Model;
using Faraday.DAL.Entities;

namespace Faraday.BL.Mapping
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class MapType : Attribute
    {
        public Type Type { get; }

        public MapType(Type type)
        {
            Type = type;
        }
    }

    public class Mapper : IMapper
    {
        private readonly ILookup<Type, MethodInfo> _methods;

        public static Mapper Instance { get; } = new Mapper();

        private Mapper()
        {
            _methods = typeof(Mapper)
                .GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Where(m => (MapType) m.GetCustomAttributes(typeof(MapType)).FirstOrDefault() != null)
                .ToLookup(
                    key => ((MapType) key.GetCustomAttributes(typeof(MapType)).First()).Type,
                    method => method
                );
        }

        public TOut Map<TOut, TIn>(TIn item, bool enter = true)
        {
            var method = _methods[typeof(TIn)].First();
            return (TOut) method.Invoke(this, new object[] {item, enter});
        }

        [MapType(typeof(CommentModel))]
        public Comment MapToEntity(CommentModel comment, bool enter = true)
        {
            if (comment is null)
            {
                return null;
            }

            return new Comment
            {
                Id = comment.Id,
                Body = comment.Body,
                CreatedAt = comment.CreatedAt,
                CreatedById = comment.CreatedById,
                CreatedBy = enter ? MapToEntity(comment.CreatedBy, false) : null,
                ModifiedAt = comment.ModifiedAt,
                IsModified = comment.IsModified,
                ParentId = comment.ParentId,
                Parent = enter ? MapToEntity(comment.Parent, false) : null
            };
        }

        [MapType(typeof(Comment))]
        public CommentModel MapToModel(Comment comment, bool enter = true)
        {
            if (comment is null)
            {
                return null;
            }

            return new CommentModel
            {
                Id = comment.Id,
                Body = comment.Body,
                CreatedAt = comment.CreatedAt,
                CreatedById = comment.CreatedById,
                CreatedBy = enter ? MapToModel(comment.CreatedBy, false) : null,
                ModifiedAt = comment.ModifiedAt,
                IsModified = comment.IsModified,
                ParentId = comment.ParentId,
                Parent = enter ? MapToModel(comment.Parent, false) : null
            };
        }

        [MapType(typeof(PostModel))]
        public Post MapToEntity(PostModel post, bool enter = true)
        {
            if (post is null)
            {
                return null;
            }

            return new Post
            {
                Id = post.Id,
                Title = post.Title,
                Body = post.Body,
                CreatedAt = post.CreatedAt,
                CreatedById = post.CreatedById,
                CreatedBy = enter ? MapToEntity(post.CreatedBy, false) : null,
                ModifiedAt = post.ModifiedAt,
                IsModified = post.IsModified,
                TeamId = post.TeamId,
                Team = enter ? MapToEntity(post.Team, false) : null
            };
        }

        [MapType(typeof(Post))]
        public PostModel MapToModel(Post post, bool enter = true)
        {
            if (post is null)
            {
                return null;
            }

            return new PostModel
            {
                Id = post.Id,
                Title = post.Title,
                Body = post.Body,
                CreatedAt = post.CreatedAt,
                CreatedById = post.CreatedById,
                CreatedBy = enter ? MapToModel(post.CreatedBy, false) : null,
                ModifiedAt = post.ModifiedAt,
                IsModified = post.IsModified,
                TeamId = post.TeamId,
                Team = enter ? MapToModel(post.Team, false) : null
            };
        }

        [MapType(typeof(UserModel))]
        public User MapToEntity(UserModel user, bool enter = true)
        {
            if (user is null)
            {
                return null;
            }

            return new User
            {
                Id = user.Id,
                CreatedAt = user.CreatedAt,
                Email = user.Email,
                Login = user.Login,
                Name = user.Name,
                Password = user.Password,
                AvatarId = user.PictureId,
                Picture = enter ? MapToEntity(user.Picture, false) : null
            };
        }

        [MapType(typeof(User))]
        public UserModel MapToModel(User user, bool enter = true)
        {
            if (user is null)
            {
                return null;
            }

            return new UserModel
            {
                Id = user.Id,
                CreatedAt = user.CreatedAt,
                Email = user.Email,
                Login = user.Login,
                Name = user.Name,
                Password = user.Password,
                PictureId = user.AvatarId,
                Picture = enter ? MapToModel(user.Picture, false) : null
            };
        }

        [MapType(typeof(TeamModel))]
        public Team MapToEntity(TeamModel team, bool enter = true)
        {
            if (team is null)
            {
                return null;
            }

            return new Team
            {
                Id = team.Id,
                Name = team.Name,
                Description = team.Description,
                CreatedAt = team.CreatedAt,
                CreatedById = team.CreatedById,
                CreatedBy = enter ? MapToEntity(team.CreatedBy, false) : null,
                ModifiedAt = team.ModifiedAt,
                AvatarId = team.PictureId,
                Picture = enter ? MapToEntity(team.Picture, false) : null
            };
        }

        [MapType(typeof(Team))]
        public TeamModel MapToModel(Team team, bool enter = true)
        {
            if (team is null)
            {
                return null;
            }

            return new TeamModel
            {
                Id = team.Id,
                Name = team.Name,
                Description = team.Description,
                CreatedAt = team.CreatedAt,
                CreatedById = team.CreatedById,
                CreatedBy = enter ? MapToModel(team.CreatedBy, false) : null,
                ModifiedAt = team.ModifiedAt,
                PictureId = team.AvatarId,
                Picture = enter ? MapToModel(team.Picture, false) : null
            };
        }

        [MapType(typeof(Avatar))]
        public AvatarModel MapToModel(Avatar avatar, bool enter = true)
        {
            if (avatar is null)
            {
                return null;
            }

            return new AvatarModel
            {
                Id = avatar.Id,
                ProfilePicture = avatar.ProfilePicture,
                CreatedAt = avatar.CreatedAt,
                CreatedById = avatar.CreatedById,
                CreatedBy = enter ? MapToModel(avatar.CreatedBy, false) : null
            };
        }

        [MapType(typeof(AvatarModel))]
        public Avatar MapToEntity(AvatarModel avatar, bool enter = true)
        {
            if (avatar is null)
            {
                return null;
            }

            return new Avatar
            {
                Id = avatar.Id,
                ProfilePicture = avatar.ProfilePicture,
                CreatedAt = avatar.CreatedAt,
                CreatedById = avatar.CreatedById,
                CreatedBy = enter ? MapToEntity(avatar.CreatedBy, false) : null,
            };
        }

        [MapType(typeof(TeamMemberModel))]
        public TeamMember MapToEntity(TeamMemberModel teamMember, bool enter = true)
        {
            if (teamMember is null)
            {
                return null;
            }

            return new TeamMember
            {
                CreatedAt = teamMember.CreatedAt,
                CreatedById = teamMember.CreatedById,
                CreatedBy = enter ? MapToEntity(teamMember.CreatedBy, false) : null,
                TeamId = teamMember.TeamId,
                Team = enter ? MapToEntity(teamMember.Team, false) : null,
                MemberId = teamMember.MemberId,
                Member = enter ? MapToEntity(teamMember.Member, false) : null
            };
        }

        [MapType(typeof(TeamMember))]
        public TeamMemberModel MapToModel(TeamMember teamMember, bool enter = true)
        {
            if (teamMember is null)
            {
                return null;
            }

            return new TeamMemberModel
            {
                CreatedAt = teamMember.CreatedAt,
                CreatedById = teamMember.CreatedById,
                CreatedBy = enter ? MapToModel(teamMember.CreatedBy, false) : null,
                TeamId = teamMember.TeamId,
                Team = enter ? MapToModel(teamMember.Team, false) : null,
                MemberId = teamMember.MemberId,
                Member = enter ? MapToModel(teamMember.Member, false) : null
            };
        }

        [MapType(typeof(ActivityModel))]
        public Activity MapToEntity(ActivityModel activity, bool enter = true)
        {
            if (activity is null)
            {
                return null;
            }

            return new Activity
            {
                Id = activity.Id,
                CreatedAt = activity.CreatedAt,
                CreatedById = activity.CreatedById,
                CreatedBy = enter ? MapToEntity(activity.CreatedBy, false) : null,
                TeamId = activity.TeamId,
                UserId = activity.UserId,
                AvatarId = activity.AvatarId,
                PostId = activity.PostId,
                CommentId = activity.CommentId,
                TeamMemberId = activity.TeamMemberId,
                UserMemberId = activity.UserMemberId,
                ActivityType = activity.Type
            };
        }

        [MapType(typeof(Activity))]
        public ActivityModel MapToModel(Activity activity, bool enter = true)
        {
            if (activity is null)
            {
                return null;
            }

            return new ActivityModel
            {
                Id = activity.Id,
                CreatedAt = activity.CreatedAt,
                CreatedById = activity.CreatedById,
                CreatedBy = enter ? MapToModel(activity.CreatedBy, false) : null,
                TeamId = activity.TeamId,
                UserId = activity.UserId,
                AvatarId = activity.AvatarId,
                PostId = activity.PostId,
                CommentId = activity.CommentId,
                TeamMemberId = activity.TeamMemberId,
                UserMemberId = activity.UserMemberId,
                Type = activity.ActivityType
            };
        }
    }
}