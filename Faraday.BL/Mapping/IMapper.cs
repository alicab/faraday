﻿using Faraday.BL.Model;
using Faraday.DAL.Entities;

namespace Faraday.BL.Mapping
{
    public interface IMapper
    {
        TOut Map<TOut, TIn>(TIn item, bool enter = true);

        Comment MapToEntity(CommentModel comment, bool enter = true);
        CommentModel MapToModel(Comment comment, bool enter = true);

        Post MapToEntity(PostModel post, bool enter = true);
        PostModel MapToModel(Post post, bool enter = true);

        User MapToEntity(UserModel user, bool enter = true);
        UserModel MapToModel(User user, bool enter = true);

        Team MapToEntity(TeamModel team, bool enter = true);
        TeamModel MapToModel(Team team, bool enter = true);

        Avatar MapToEntity(AvatarModel avatar, bool enter = true);
        AvatarModel MapToModel(Avatar avatar, bool enter = true);

        TeamMember MapToEntity(TeamMemberModel teamMember, bool enter = true);
        TeamMemberModel MapToModel(TeamMember teamMember, bool enter = true);

        Activity MapToEntity(ActivityModel activity, bool enter = true);
        ActivityModel MapToModel(Activity activity, bool enter = true);
    }
}