﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Faraday.BL.Model
{
    public abstract class ContributionModel
    {
        public int Id { get; set; }

        [StringLength(4096)] public string Body { get; set; }

        public DateTime? CreatedAt { get; set; }

        public int? CreatedById { get; set; }

        public UserModel CreatedBy { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public bool IsModified { get; set; }
    }
}