﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Faraday.BL.Model
{
    public class TeamModel
    {
        public int Id { get; set; }

        [StringLength(256)] public string Name { get; set; }

        [StringLength(4096)] public string Description { get; set; }

        public DateTime? CreatedAt { get; set; }

        public int? CreatedById { get; set; }

        public UserModel CreatedBy { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public int? PictureId { get; set; }

        public AvatarModel Picture { get; set; }

        public ICollection<TeamMemberModel> Members { get; set; }

        public ICollection<PostModel> Posts { get; set; }
    }
}