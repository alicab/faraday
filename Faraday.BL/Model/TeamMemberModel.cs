using System;

namespace Faraday.BL.Model
{
    public class TeamMemberModel
    {
        public DateTime? CreatedAt { get; set; }

        public int? CreatedById { get; set; }

        public UserModel CreatedBy { get; set; }

        public int TeamId { get; set; }

        public TeamModel Team { get; set; }

        public int MemberId { get; set; }

        public UserModel Member { get; set; }
    }
}