﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Faraday.BL.Model
{
    public class UserModel
    {
        public int Id { get; set; }

        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// http://www.rfc-editor.org/errata/eid1690
        /// </summary>
        [StringLength(256)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [StringLength(64)] public string Login { get; set; }

        [StringLength(128)] public string Name { get; set; }

        [StringLength(128)] public string Password { get; set; }

        public int? PictureId { get; set; }

        public AvatarModel Picture { get; set; }

        public ICollection<TeamMemberModel> MemberOf { get; set; }

        public ICollection<ActivityModel> Activities { get; set; }
    }
}