﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Faraday.BL.Model
{
    public class PostModel : ContributionModel
    {
        [StringLength(512)] public string Title { get; set; }

        public int? TeamId { get; set; }

        public TeamModel Team { get; set; }

        public ICollection<CommentModel> Children { get; set; }
    }
}