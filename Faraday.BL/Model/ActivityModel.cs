using System;
using Faraday.DAL.Entities;

namespace Faraday.BL.Model
{
    public class ActivityModel
    {
        public int Id { get; set; }

        public DateTime? CreatedAt { get; set; }

        public int? CreatedById { get; set; }

        public UserModel CreatedBy { get; set; }

        public int? TeamId { get; set; }

        public int? UserId { get; set; }

        public int? AvatarId { get; set; }

        public int? PostId { get; set; }

        public int? CommentId { get; set; }

        public int? TeamMemberId { get; set; }

        public int? UserMemberId { get; set; }

        public Activity.Type Type { get; set; }
    }
}