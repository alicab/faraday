using System.Collections.Generic;

namespace Faraday.BL.Model
{
    public class CommentModel : ContributionModel
    {
        public int? ParentId { get; set; }

        public PostModel Parent { get; set; }
    }
}