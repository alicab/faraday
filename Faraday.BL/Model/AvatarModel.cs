using System;

namespace Faraday.BL.Model
{
    public class AvatarModel
    {
        public int Id { get; set; }

        public byte[] ProfilePicture { get; set; }

        public DateTime? CreatedAt { get; set; }

        public int? CreatedById { get; set; }

        public UserModel CreatedBy { get; set; }
    }
}