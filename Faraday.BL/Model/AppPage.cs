﻿namespace Faraday.BL.Model
{
    public enum AppPage
    {
        Login,
        Register,
        HomeView,
        UserDetail,
        TeamDetail,
        TeamCreation,
    }
}