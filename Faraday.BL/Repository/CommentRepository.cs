using System.Collections.Generic;
using System.Linq;
using Faraday.BL.Model;
using Faraday.DAL;
using Faraday.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Faraday.BL.Repository
{
    public class CommentRepository : Repository<Comment, CommentModel>
    {
        public CommentRepository(IDbContextFactory appDbContextFactory) : base(appDbContextFactory)
        {
        }

        public CommentModel Search(string text)
        {
            return Db(context => Mapper
                .MapToModel(context.Comments
                    .Include(c => c.CreatedBy)
                    .FirstOrDefault(p => p.Body.Contains(text))
                )
            );
        }

        public void LoadAuthor(CommentModel model)
        {
            var entity = Mapper.MapToEntity(model);
            var comment = Db(context => context.Comments.Include(a => a.CreatedBy).First(a => a == entity));
            model.CreatedBy = Mapper.MapToModel(comment.CreatedBy);
        }

        public ICollection<CommentModel> GetAllByParent(PostModel parent)
        {
            var entity = Mapper.MapToEntity(parent);
            return Db(context => context.Comments
                .Include(c => c.CreatedBy)
                .Include(c => c.Parent)
                .Where(x => x.Parent == entity)
                .Select(x => Mapper.MapToModel(x, true))
                .ToList()
            );
        }

        public ICollection<CommentModel> GetAllByParent(int parentId)
        {
            return Db(context => context.Comments
                .Include(c => c.CreatedBy)
                .Include(c => c.Parent)
                .Where(x => x.ParentId == parentId)
                .Select(x => Mapper.MapToModel(x, true))
                .ToList()
            );
        }

        public ICollection<CommentModel> GetAllByAuthor(UserModel createdBy)
        {
            var entity = Mapper.MapToEntity(createdBy);
            return Db(context => context.Comments
                .Include(c => c.CreatedBy)
                .Where(x => x.CreatedBy == entity)
                .Select(x => Mapper.MapToModel(x, true))
                .ToList()
            );
        }

        public ICollection<CommentModel> GetAllByAuthor(int createdById)
        {
            return Db(context => context.Comments
                .Include(c => c.CreatedBy)
                .Where(x => x.CreatedById == createdById)
                .Select(x => Mapper.MapToModel(x, true))
                .ToList()
            );
        }

        public new CommentModel Update(CommentModel item)
        {
            return Db(context =>
            {
                var entity = Mapper.MapToEntity(item);
                entity.IsModified = true;
                var record = context.Comments.Update(entity);
                context.SaveChanges();
                ActivityUpdate(context, entity);
                return Mapper.MapToModel(record.Entity);
            });
        }

        protected override void ActivityInsert(AppDbContext context, Comment entity)
        {
            var activity = CreateActivity(Activity.Type.Insert, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        protected override void ActivityUpdate(AppDbContext context, Comment entity)
        {
            var activity = CreateActivity(Activity.Type.Update, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        protected override void ActivityDelete(AppDbContext context, Comment entity)
        {
            var activity = CreateActivity(Activity.Type.Delete, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        private static Activity CreateActivity(Activity.Type type, Comment entity)
        {
            return new Activity
            {
                ActivityType = type,
                CreatedById = entity.CreatedById,
                CommentId = entity.Id
            };
        }
    }
}