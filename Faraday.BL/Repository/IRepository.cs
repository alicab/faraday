using System.Collections.Generic;

namespace Faraday.BL.Repository
{
    public interface IRepository<T>
    {
        ICollection<T> GetAll();

        T GetById(int id);

        T Insert(T item);

        T Update(T item);

        T Remove(int id);

        T Remove(T item);
    }
}