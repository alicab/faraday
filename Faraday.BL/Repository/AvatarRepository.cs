using Faraday.BL.Model;
using Faraday.DAL;
using Faraday.DAL.Entities;

namespace Faraday.BL.Repository
{
    public class AvatarRepository : Repository<Avatar, AvatarModel>
    {
        public AvatarRepository(IDbContextFactory appDbContextFactory) : base(appDbContextFactory)
        {
        }

        protected override void ActivityInsert(AppDbContext context, Avatar entity)
        {
            var activity = CreateActivity(Activity.Type.Insert, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        protected override void ActivityUpdate(AppDbContext context, Avatar entity)
        {
            var activity = CreateActivity(Activity.Type.Update, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        protected override void ActivityDelete(AppDbContext context, Avatar entity)
        {
            var activity = CreateActivity(Activity.Type.Delete, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        private static Activity CreateActivity(Activity.Type type, Avatar entity)
        {
            return new Activity
            {
                ActivityType = type,
                CreatedById = entity.CreatedById,
                AvatarId = entity.Id
            };
        }
    }
}