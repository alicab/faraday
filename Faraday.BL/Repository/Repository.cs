﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faraday.BL.Mapping;
using Faraday.DAL;
using Faraday.DAL.Entities.Base;
using Microsoft.EntityFrameworkCore;

namespace Faraday.BL.Repository
{
    public abstract class Repository<TEntity, TModel> : IRepository<TModel> where TEntity : EntityId
    {
        private readonly IDbContextFactory _appDbContextFactory;
        protected readonly IMapper Mapper = Mapping.Mapper.Instance;

        protected Repository(IDbContextFactory dbContextFactory)
        {
            _appDbContextFactory = dbContextFactory;
        }

        protected void Db(Action<AppDbContext> action)
        {
            using (var appDbContext = _appDbContextFactory.CreateDbContext())
            {
                action(appDbContext);
            }
        }

        protected TResult Db<TResult>(Func<AppDbContext, TResult> action)
        {
            using (var appDbContext = _appDbContextFactory.CreateDbContext())
            {
                return action(appDbContext);
            }
        }

        protected virtual void ActivityInsert(AppDbContext context, TEntity entity)
        {
        }

        protected virtual void ActivityUpdate(AppDbContext context, TEntity entity)
        {
        }

        protected virtual void ActivityDelete(AppDbContext context, TEntity entity)
        {
        }

        public ICollection<TModel> GetAll()
        {
            return Db(context => context.Set<TEntity>().Select(x => Mapper.Map<TModel, TEntity>(x, true)).ToList());
        }

        public TModel GetById(int id)
        {
            return Db(context => Mapper
                .Map<TModel, TEntity>(context.Set<TEntity>().FirstOrDefault(i => i.Id == id))
            );
        }

        private static TEntity Attach(DbContext context, TEntity entity, EntityState state)
        {
            var record = context.Set<TEntity>().Attach(entity);
            record.State = state;
            context.SaveChanges();
            return record.Entity;
        }

        public TModel Insert(TModel item)
        {
            return Db(context =>
            {
                var entity = Mapper.Map<TEntity, TModel>(item);
                var record = Attach(context, entity, EntityState.Added);
                //ActivityInsert(context, record);
                return Mapper.Map<TModel, TEntity>(record);
            });
        }

        public TModel Update(TModel item)
        {
            return Db(context =>
            {
                var entity = Mapper.Map<TEntity, TModel>(item);
                var record = Attach(context, entity, EntityState.Modified);
                //ActivityUpdate(context, record);
                return Mapper.Map<TModel, TEntity>(record);
            });
        }

        public TModel Remove(int id)
        {
            return Db(context =>
            {
                var entity = context.Set<TEntity>().FirstOrDefault(i => i.Id == id);
                if (entity != null)
                {
                    //ActivityDelete(context, entity);
                    entity = Attach(context, entity, EntityState.Deleted);
                }

                return Mapper.Map<TModel, TEntity>(entity);
            });
        }

        public TModel Remove(TModel item)
        {
            return Db(context =>
            {
                ActivityDelete(context, Mapper.Map<TEntity, TModel>(item));
                var entity = Mapper.Map<TEntity, TModel>(item);
                var record = Attach(context, entity, EntityState.Deleted);
                return Mapper.Map<TModel, TEntity>(record);
            });
        }
    }
}