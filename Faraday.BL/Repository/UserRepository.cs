using System.Linq;
using Faraday.BL.Model;
using Faraday.DAL;
using Faraday.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Faraday.BL.Repository
{
    public class UserRepository : Repository<User, UserModel>
    {
        public UserRepository(IDbContextFactory dbContextFactory) : base(dbContextFactory)
        {
        }

        public bool EmailExists(string email)
        {
            var user = Db(context => context.Users.FirstOrDefault(u => u.Email == email));
            return user != null;
        }

        public bool LoginExists(string login)
        {
            var user = Db(context => context.Users.FirstOrDefault(u => u.Login == login));
            return user != null;
        }

        public void LoadAvatar(UserModel model)
        {
            var entity = Mapper.MapToEntity(model);
            var user = Db(context => context.Users
                .Include(u => u.Picture)
                .First(u => u == entity)
            );
            model.Picture = Mapper.MapToModel(user.Picture);
        }

        public void LoadTeams(UserModel model)
        {
            var entity = Mapper.MapToEntity(model);
            var user = Db(context => context.Users
                .Include(u => u.MemberOf)
                .ThenInclude(tm => tm.Team)
                .First(u => u == entity)
            );
            model.MemberOf = user?.MemberOf?.Select(x => Mapper.MapToModel(x)).ToList();
        }

        public void LoadActivities(UserModel model)
        {
            var entity = Mapper.MapToEntity(model);
            var user = Db(context => context.Users
                .Include(u => u.Activities)
                .ThenInclude(a => a.CreatedBy)
                .First(u => u == entity)
            );
            model.Activities = user?.Activities?.Select(x => Mapper.MapToModel(x)).ToList();
        }

        public UserModel GetByLogin(string login)
        {
            return Db(context => Mapper.MapToModel(context.Users.FirstOrDefault(i => i.Login == login)));
        }

        protected override void ActivityInsert(AppDbContext context, User entity)
        {
            var activity = CreateActivity(Activity.Type.Insert, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        protected override void ActivityUpdate(AppDbContext context, User entity)
        {
            var activity = CreateActivity(Activity.Type.Update, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        protected override void ActivityDelete(AppDbContext context, User entity)
        {
            var activity = CreateActivity(Activity.Type.Delete, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        private static Activity CreateActivity(Activity.Type type, User entity)
        {
            return new Activity
            {
                ActivityType = type,
                CreatedById = entity.Id,
                UserId = entity.Id
            };
        }
    }
}