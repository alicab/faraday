﻿using System.Collections.Generic;
using System.Linq;
using Faraday.BL.Model;
using Faraday.DAL;
using Faraday.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Faraday.BL.Repository
{
    public class ActivityRepository : Repository<Activity, ActivityModel>
    {
        public ActivityRepository(IDbContextFactory dbContextFactory) : base(dbContextFactory)
        {
        }

        public void LoadAuthor(ActivityModel model)
        {
            var entity = Mapper.MapToEntity(model);
            var activity = Db(context => context.Activities
                .Include(a => a.CreatedBy)
                .First(a => a == entity)
            );
            model.CreatedBy = Mapper.MapToModel(activity.CreatedBy);
        }

        public ActivityModel GetLastByUser(UserModel model)
        {
            var entity = Mapper.MapToEntity(model);
            return Db(context => Mapper
                .MapToModel(context.Activities
                    .Include(a => a.CreatedBy)
                    .OrderByDescending(a => a.Id)
                    .FirstOrDefault(a => a.CreatedBy == entity)
                )
            );
        }

        public ICollection<ActivityModel> GetLastByUser(UserModel model, int count)
        {
            var entity = Mapper.MapToEntity(model);
            return Db(context => context.Activities
                .Include(a => a.CreatedBy)
                .Where(a => a.CreatedBy == entity)
                .OrderByDescending(a => a.Id)
                .Take(count)
                .Select(x => Mapper.MapToModel(x, true))
                .ToList()
            );
        }
    }
}