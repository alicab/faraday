using System.Collections.Generic;
using System.Linq;
using Faraday.BL.Model;
using Faraday.DAL;
using Faraday.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Faraday.BL.Repository
{
    public class PostRepository : Repository<Post, PostModel>
    {
        public PostRepository(IDbContextFactory appDbContextFactory) : base(appDbContextFactory)
        {
        }

        public PostModel SearchInTitle(string text)
        {
            return Db(context => Mapper
                .MapToModel(context.Posts
                    .Include(p => p.CreatedBy)
                    .FirstOrDefault(p => p.Title.Contains(text))
                )
            );
        }

        public PostModel SearchInBody(string text)
        {
            return Db(context => Mapper
                .MapToModel(context.Posts
                    .Include(p => p.CreatedBy)
                    .FirstOrDefault(p => p.Body.Contains(text))
                )
            );
        }

        public PostModel Search(string text)
        {
            return Db(context => Mapper
                .MapToModel(context.Posts
                    .Include(p => p.CreatedBy)
                    .FirstOrDefault(p => p.Title.Contains(text) || p.Body.Contains(text))
                )
            );
        }

        public void LoadAuthor(PostModel model)
        {
            var entity = Mapper.MapToEntity(model);
            var post = Db(context => context.Posts
                .Include(a => a.CreatedBy)
                .First(a => a == entity)
            );
            model.CreatedBy = Mapper.MapToModel(post.CreatedBy);
        }

        public void LoadChildrenWithAuthor(PostModel model)
        {
            var entity = Mapper.MapToEntity(model);
            var post = Db(context => context.Posts
                .Include(p => p.Children)
                .ThenInclude(c => c.CreatedBy)
                .First(p => p == entity)
            );
            model.Children = post.Children?.Select(x => Mapper.MapToModel(x)).ToList();
        }

        public new PostModel Update(PostModel item)
        {
            return Db(context =>
            {
                var entity = Mapper.MapToEntity(item);
                entity.IsModified = true;
                var record = context.Posts.Update(entity);
                context.SaveChanges();
                ActivityUpdate(context, entity);
                return Mapper.MapToModel(record.Entity);
            });
        }

        public ICollection<PostModel> GetAllByTeam(TeamModel team)
        {
            var entity = Mapper.MapToEntity(team);
            return Db(context => context.Posts
                .Include(p => p.CreatedBy)
                .Include(p => p.Team)
                .Where(x => x.Team == entity)
                .Select(x => Mapper.MapToModel(x, true))
                .ToList()
            );
        }

        public ICollection<PostModel> GetAllByTeam(int teamId)
        {
            return Db(context => context.Posts
                .Include(p => p.CreatedBy)
                .Include(p => p.Team)
                .Where(x => x.TeamId == teamId)
                .Select(x => Mapper.MapToModel(x, true))
                .ToList()
            );
        }

        public ICollection<PostModel> GetAllByAuthor(UserModel createdBy)
        {
            var entity = Mapper.MapToEntity(createdBy);
            return Db(context => context.Posts
                .Include(p => p.CreatedBy)
                .Include(p => p.Team)
                .Where(x => x.CreatedBy == entity)
                .Select(x => Mapper.MapToModel(x, true))
                .ToList()
            );
        }

        public ICollection<PostModel> GetAllByAuthor(int createdById)
        {
            return Db(context => context.Posts
                .Include(p => p.CreatedBy)
                .Include(p => p.Team)
                .Where(x => x.CreatedById == createdById)
                .Select(x => Mapper.MapToModel(x, true))
                .ToList()
            );
        }

        protected override void ActivityInsert(AppDbContext context, Post entity)
        {
            var activity = CreateActivity(Activity.Type.Insert, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        protected override void ActivityUpdate(AppDbContext context, Post entity)
        {
            var activity = CreateActivity(Activity.Type.Update, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        protected override void ActivityDelete(AppDbContext context, Post entity)
        {
            var activity = CreateActivity(Activity.Type.Delete, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        private static Activity CreateActivity(Activity.Type type, Post entity)
        {
            return new Activity
            {
                ActivityType = type,
                CreatedById = entity.CreatedById,
                PostId = entity.Id
            };
        }
    }
}