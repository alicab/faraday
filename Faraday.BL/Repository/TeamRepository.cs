using System.Linq;
using Faraday.BL.Model;
using Faraday.DAL;
using Faraday.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Faraday.BL.Repository
{
    public class TeamRepository : Repository<Team, TeamModel>
    {
        public TeamRepository(IDbContextFactory dbContextFactory) : base(dbContextFactory)
        {
        }

        public void LoadTeamMembers(TeamModel model)
        {
            var entity = Mapper.MapToEntity(model);
            var team = Db(context => context.Teams
                .Include(p => p.CreatedBy)
                .Include(t => t.Members)
                .ThenInclude(tm => tm.Member)
                .First(t => t == entity)
            );
            model.Members = team.Members?.Select(x => Mapper.MapToModel(x)).ToList();
        }

        public void LoadPosts(TeamModel model)
        {
            var entity = Mapper.MapToEntity(model);
            var team = Db(context => context.Teams
                .Include(p => p.CreatedBy)
                .Include(t => t.Posts)
                .First(t => t == entity)
            );
            model.Posts = team.Posts?.Select(x => Mapper.MapToModel(x)).ToList();
        }

        protected override void ActivityInsert(AppDbContext context, Team entity)
        {
            var activity = CreateActivity(Activity.Type.Insert, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        protected override void ActivityUpdate(AppDbContext context, Team entity)
        {
            var activity = CreateActivity(Activity.Type.Update, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        protected override void ActivityDelete(AppDbContext context, Team entity)
        {
            var activity = CreateActivity(Activity.Type.Delete, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        private static Activity CreateActivity(Activity.Type type, Team entity)
        {
            return new Activity
            {
                ActivityType = type,
                CreatedById = entity.CreatedById,
                TeamId = entity.Id
            };
        }
    }
}