using System.Collections.Generic;
using System.Linq;
using Faraday.BL.Model;
using Faraday.DAL;
using Faraday.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Faraday.BL.Repository
{
    public class TeamMemberRepository : Repository<TeamMember, TeamMemberModel>
    {
        public TeamMemberRepository(IDbContextFactory dbContextFactory) : base(dbContextFactory)
        {
        }

        public new TeamMemberModel GetById(int id)
        {
            throw new System.NotSupportedException();
        }

        public TeamMemberModel GetById(int userId, int teamId)
        {
            return Db(context => Mapper.MapToModel(context.TeamMembers
                .FirstOrDefault(t => t.TeamId == teamId && t.MemberId == userId)
            ));
        }

        public ICollection<TeamMemberModel> GetAllByUserId(int userId)
        {
            return Db(context => context.TeamMembers
                .Where(i => i.MemberId == userId)
                .Include(x => x.CreatedBy)
                .Include(x => x.Member)
                .Include(x => x.Team)
                .Select(x => Mapper.MapToModel(x, true))
                .ToList());
        }

        public ICollection<TeamMemberModel> GetAllByTeamId(int teamId)
        {
            return Db(context => context.TeamMembers
                .Where(i => i.TeamId == teamId)
                .Include(x => x.CreatedBy)
                .Include(x => x.Member)
                .Include(x => x.Team)
                .Select(x => Mapper.MapToModel(x, true))
                .ToList()
            );
        }

        public new void Remove(int id)
        {
            throw new System.NotSupportedException();
        }

        public void Remove(int userId, int teamId)
        {
            Db(context =>
            {
                var entity = context.TeamMembers.FirstOrDefault(t => t.TeamId == teamId && t.MemberId == userId);
                if (entity == null)
                {
                    return;
                }

                context.TeamMembers.Remove(entity);
                context.SaveChanges();
            });
        }

        protected override void ActivityInsert(AppDbContext context, TeamMember entity)
        {
            var activity = CreateActivity(Activity.Type.Insert, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        protected override void ActivityUpdate(AppDbContext context, TeamMember entity)
        {
            var activity = CreateActivity(Activity.Type.Update, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        protected override void ActivityDelete(AppDbContext context, TeamMember entity)
        {
            var activity = CreateActivity(Activity.Type.Delete, entity);
            context.Activities.Add(activity);
            context.SaveChanges();
        }

        private static Activity CreateActivity(Activity.Type type, TeamMember entity)
        {
            return new Activity
            {
                ActivityType = type,
                CreatedById = entity.CreatedById,
                TeamMemberId = entity.TeamId,
                UserMemberId = entity.MemberId
            };
        }
    }
}