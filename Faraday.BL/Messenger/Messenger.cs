﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace Faraday.BL.Messenger
{
    public class Messenger : IMessenger
    {
        private readonly
            ConcurrentDictionary<Type, ConcurrentBag<Delegate>> _registeredActions
                = new ConcurrentDictionary<Type, ConcurrentBag<Delegate>>();

        private readonly object _backLock = new object();

        public static Messenger Instance { get; } = new Messenger();

        private Messenger()
        {
        }

        public void Register<TMessage>(Action<TMessage> action)
        {
            var key = typeof(TMessage);

            lock (_backLock)
            {
                if (!_registeredActions.TryGetValue(typeof(TMessage), out var actions))
                {
                    actions = new ConcurrentBag<Delegate>();
                    _registeredActions[key] = actions;
                }

                actions.Add(action);
            }
        }

        public void Send<TMessage>(TMessage message)
        {
            lock (_backLock)
            {
                if (_registeredActions.TryGetValue(typeof(TMessage), out var actions))
                {
                    foreach (var action in actions.Select(a => a as Action<TMessage>).Where(a => a != null))
                    {
                        action(message);
                    }
                }
            }
        }

        public void UnRegister<TMessage>(Action<TMessage> action)
        {
            var key = typeof(TMessage);

            lock (_backLock)
            {
                if (_registeredActions.TryGetValue(typeof(TMessage), out var actions))
                {
                    var actionsList = actions.ToList();
                    actionsList.Remove(action);
                    _registeredActions[key] = new ConcurrentBag<Delegate>(actionsList);
                }
            }
        }
    }
}