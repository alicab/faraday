﻿namespace Faraday.BL.Messenger.Messages
{
    public class RegistrationMessage
    {
        public class Login
        {
            public abstract class AbstractRegistrationMessage
            {
                public string NewLogin { get; set; }
            }

            public class Save : AbstractRegistrationMessage
            {
            }

            public class Request
            {
            }

            public class Response : AbstractRegistrationMessage
            {
            }
        }
    }
}