﻿using Faraday.BL.Model;

namespace Faraday.BL.Messenger.Messages
{
    public abstract class PageMessage
    {
        public class GoTo
        {
            public AppPage Page { get; set; }
        }

        public class Next
        {
            public object Page { get; set; }
        }
    }
}