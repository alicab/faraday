﻿using Faraday.BL.Model;

namespace Faraday.BL.Messenger.Messages
{
    public class UserSelectedMessage
    {
        public UserModel User { get; set; }
        public bool IsMember { get; set; }
    }
}