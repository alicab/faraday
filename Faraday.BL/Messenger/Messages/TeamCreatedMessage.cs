﻿using Faraday.BL.Model;

namespace Faraday.BL.Messenger.Messages
{
    public class TeamCreatedMessage
    {
        public TeamModel NewTeam { get; set; }
    }
}