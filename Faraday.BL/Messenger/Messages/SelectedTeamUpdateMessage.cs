﻿using Faraday.BL.Model;

namespace Faraday.BL.Messenger.Messages
{
    public class SelectedTeamUpdateMessage
    {
        public abstract class AbstractSelectedTeamUpdateMessage
        {
            public TeamModel SelectedTeam { get; set; }
        }

        public class Response : AbstractSelectedTeamUpdateMessage
        {
        }

        public class Update : AbstractSelectedTeamUpdateMessage
        {
        }

        public class Request
        {
        }
    }
}