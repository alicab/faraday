﻿using Faraday.BL.Model;

namespace Faraday.BL.Messenger.Messages
{
    public abstract class UserMessage
    {
        public abstract class AbstractUserMessage
        {
            public UserModel User { get; set; }
        }

        public class Login : AbstractUserMessage
        {
        }

        public class Logout
        {
        }

        public class Request
        {
        }

        public class Response : AbstractUserMessage
        {
        }
    }
}