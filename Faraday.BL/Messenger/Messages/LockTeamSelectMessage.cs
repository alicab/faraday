﻿namespace Faraday.BL.Messenger.Messages
{
    public class LockTeamSelectMessage
    {
        public bool IsTeamLocked { get; set; }
    }
}