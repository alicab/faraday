﻿namespace Faraday.BL.Messenger.Messages
{
    public enum ViewModelId
    {
        MainWindowId,
        UserPreviewId,
        TeamDetailId,
        PostListId,
        CommentListId,
        UserDetailId,
        TeamCreationId,
        UserListId
    }
}