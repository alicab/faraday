﻿using Faraday.BL.Model;

namespace Faraday.BL.Messenger.Messages
{
    public class SelectedPostUpdateMessage
    {
        public PostModel SelectedPost { get; set; }
    }
}